﻿
namespace AST.Enums
{
    public enum LogEventLevel
    {
        Info,

        Warning,

        Error
    }
}
