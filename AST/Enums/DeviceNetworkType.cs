﻿
using System.ComponentModel;

namespace AST.Enums
{
    public enum DeviceNetworkType
    {
        [Description("3G")]
        G3,

        [Description("4G")]
        G4,

        [Description("4G & LTE")]
        G4_LTE
    }
}
