﻿
using System.ComponentModel;

namespace AST.Enums
{
    public enum ScenarioType
    {
        [Description("Registration")]
        REG,

        [Description("Simple Call")]
        SIM,

        [Description("Conference")]
        CON
    }
}
