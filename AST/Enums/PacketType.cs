﻿
using System.ComponentModel;

namespace AST.Enums
{
    public enum PacketType
    {
        [Description("INVITE")]
        INVITE,

        [Description("ACK")]
        ACK,

        [Description("BYE")]
        BYE,

        [Description("MESSAGE")]
        MESSAGE,

        [Description("PRACK")]
        PRACK,

        [Description("UPDATE")]
        UPDATE,

        //[Description("SUBSCRIBE")]
        //SUBSCRIBE,

        //[Description("NOTIFY")]
        //NOTIFY,

        [Description("CANCEL")]
        CANCEL,

        [Description("100")]
        _100,

        [Description("180")]
        _180,

        [Description("183")]
        _183,

        [Description("200")]
        _200,

        [Description("202")]
        _202,

        [Description("4XX-6XX")]
        _4XX_6XX,

        [Description("UNKNOWN")]
        UNKNOWN
    }
}
