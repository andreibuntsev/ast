﻿
using System.ComponentModel;

namespace AST.Enums
{
    public enum NetworkElementType
    {
        [Description("P-CSCF")]
        P_CSCF,

        [Description("MTAS")]
        MTAS,

        [Description("S-CSCF")]
        S_CSCF,

        [Description("I-CSCF")]
        I_CSCF,

        [Description("AS")]
        AS,

        [Description("HSS")]
        HSS,

        [Description("MSC")]
        MSC,

        [Description("BGF")]
        BGF,

        [Description("MRFP")]
        MRFP,

        [Description("MGW")]
        MGW,

        [Description("IBCF")]
        IBCF
    }
}
