﻿using System;
using System.Collections.Generic;
using System.Linq;
using AST.Common;
using AST.DataModel;
using AST.Enums;



namespace AST.Logic
{
    public static class CallFlowAnalyzer
    {
        private static bool _IsDebugMode = true;

        /// <summary>
        /// Analyzes whole the test trace against the reference trace
        /// </summary>
        /// <param name="refPackets"></param>
        /// <param name="testPackets"></param>
        /// <param name="relatedPackets"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool Analyze(List<Packet> refPackets, List<Packet> testPackets, List<Tuple<Packet, Packet>> relatedPackets, out string message, out int? firstErrorPacketNo, out string firstErrorMesage)
        {
            try
            {
                message = null;
                firstErrorPacketNo = null;
                firstErrorMesage = null;

                if (refPackets == null || refPackets.Count == 0)
                {
                    message = "Reference call flow packet list is empty";
                    LogHelper.LogError(message);
                    return false;
                }

                LogHelper.LogInfo($"Reference call flow packet list contains {refPackets.Count} items");

                if (testPackets == null || testPackets.Count == 0)
                {
                    message = "Test call flow packet list is empty";
                    LogHelper.LogError(message);
                    return false;
                }

                LogHelper.LogInfo($"Test call flow packet list contains {testPackets.Count} items");

                //Reorganize packets to group them by CallIds
                var refCallIdPacketGroups = new Dictionary<string, List<Packet>>();
                foreach (Packet packet in refPackets)
                {
                    if (!refCallIdPacketGroups.ContainsKey(packet.CallID))
                    {
                        refCallIdPacketGroups.Add(packet.CallID, new List<Packet>());
                    }

                    refCallIdPacketGroups[packet.CallID].Add(packet);
                }

                var testCallIdPacketGroups = new Dictionary<string, List<Packet>>();
                foreach (Packet packet in testPackets)
                {
                    if (!testCallIdPacketGroups.ContainsKey(packet.CallID))
                    {
                        testCallIdPacketGroups.Add(packet.CallID, new List<Packet>());
                    }

                    testCallIdPacketGroups[packet.CallID].Add(packet);
                }


                //Check Call IDs number
                if (refCallIdPacketGroups.Count != testCallIdPacketGroups.Count)
                {
                    LogHelper.LogError($"Ref Call ID number ({refCallIdPacketGroups.Count}) is not equal to test Call ID number ({testCallIdPacketGroups.Count})");
                    if (!_IsDebugMode) return false;
                }

                LogHelper.LogInfo($"Reference trace Call ID number is {refCallIdPacketGroups.Count}");
                LogHelper.LogInfo($"Test trace Call ID number is {testCallIdPacketGroups.Count}");


                for (int i = 0; i < refCallIdPacketGroups.Count; i++)
                {
                    if (i >= testCallIdPacketGroups.Count)
                    {
                        LogHelper.LogError($"Ref Call Flow contains {refCallIdPacketGroups.Count} Call IDs number that is more than test Call IDs number {testCallIdPacketGroups.Count}");
                        return false;
                    }

                    if (!AnalyzeCallId(refCallIdPacketGroups.Keys.ToList()[i], testCallIdPacketGroups.Keys.ToList()[i],
                        refCallIdPacketGroups.Values.ToList()[i], testCallIdPacketGroups.Values.ToList()[i],
                        relatedPackets, ref message, ref firstErrorPacketNo, ref firstErrorMesage))
                    {
                        if (!_IsDebugMode) return false;
                    }
                }

                return String.IsNullOrEmpty(message);
            }
            catch (Exception ex)
            {
                message = "Error";
                firstErrorPacketNo = null;
                firstErrorMesage = "Unhandled Exception: " + ex.Message;
                return false;
            }
        }


        /// <summary>
        /// Analyzes the test trace against the reference trace
        /// </summary>
        /// <param name="testCallId"></param>
        /// <param name="refPackets"></param>
        /// <param name="testPackets"></param>
        /// <param name="relatedPackets"></param>
        /// <param name="message"></param>
        /// <param name="refCallId"></param>
        /// <returns></returns>
        private static bool AnalyzeCallId(string refCallId, string testCallId, List<Packet> refPackets, List<Packet> testPackets, List<Tuple<Packet, Packet>> relatedPackets, ref string message, ref int? firstErrorPacketNo, ref string firstErrorMesage)
        {
            string _message = null;

            LogHelper.LogInfo($"Started analyzing test Call ID '{testCallId}' against reference Call ID '{refCallId}'");

            List<NetworkElement> refNetworkElements = refPackets.SelectMany(p => new List<string>() {p.Source, p.Destination}).Distinct().Select(ip =>
            {
                NetworkElement networkElement = GlobalData.NetworkElements.FirstOrDefault(ne => ne.IPAddress == ip);
                if (networkElement == null)
                {
                    _message = $"IP {ip} not found in the Network Elements table";
                    LogHelper.LogError(_message);
                    return null;
                }

                return networkElement;
            }).ToList();
            if (_message != null)
            {
                message = _message;
                return false;
            }
            LogHelper.LogInfo($"Reference trace Call ID '{refCallId}' contains Network Elements: '{refNetworkElements.Select(ne => ne.Type).AsCsvString()}'");

            List<NetworkElement> testNetworkElements = testPackets.SelectMany(p => new List<string>() { p.Source, p.Destination }).Distinct().Select(ip =>
            {
                NetworkElement networkElement = GlobalData.NetworkElements.FirstOrDefault(ne => ne.IPAddress == ip);
                if (networkElement == null)
                {
                    _message = $"IP {ip} not found in the Network Elements table";
                    return null;
                }

                return networkElement;
            }).ToList();
            if (_message != null)
            {
                message = _message;
                return false;
            }
            LogHelper.LogInfo($"Test trace Call ID '{testCallId}' contains Network Elements: '{testNetworkElements.Select(ne => ne.Type).AsCsvString()}'");

            //Collect all ref & test used packets so that not to use them more than once
            var usedRefPackets = new List<Packet>();
            var usedTestPackets = new List<Packet>();

            foreach (NetworkElement refNetworkElement in refNetworkElements)
            {
                if (refNetworkElement.TypeEnum == NetworkElementType.I_CSCF)
                {
                    LogHelper.LogInfo($"Test trace Call ID '{testCallId}' Network Element '{refNetworkElement.Type}' has type I-CSCF so it has been skipped");
                    continue;
                }

                NetworkElement testNetworkElement = testNetworkElements.FirstOrDefault(tne => tne.TypeEnum == refNetworkElement.TypeEnum);
                if (testNetworkElement == null)
                {
                    message = $"Test trace doesn’t contain network element {refNetworkElement.Type}";
                    LogHelper.LogError(message);
                    return false;
                }
                LogHelper.LogInfo($"Ref and Test traces both contain network element {refNetworkElement.Type}");

                //Get all mandatory requests from the network element
                List<Packet> mandatoryRequests = refPackets.Where(p => 
                    p.Source == refNetworkElement.IPAddress && IsMandatoryRequest(p.PacketTypeEnum)).ToList();


                
                //----------------------------------------------------------------
                //Go through mandatory requests and compare them with test requests
                foreach (Packet mandatoryRequest in mandatoryRequests)
                {
                    LogHelper.LogInfo($"Ref trace contains mandatory request in packet #{mandatoryRequest.PacketNo}: {mandatoryRequest.PacketType}");
                    usedRefPackets.Add(mandatoryRequest);

                    if (mandatoryRequest.DestinatioNetworkElement.TypeEnum == NetworkElementType.S_CSCF
                        && mandatoryRequest.SourceNetworkElement.TypeEnum == NetworkElementType.S_CSCF)
                    {
                        LogHelper.LogWarning($"Ref trace contains '{mandatoryRequest.PacketType}' #{mandatoryRequest.PacketNo} from S_SCCF to S_SCCF which is ignored");
                        continue;
                    }

                    //Get a similar mandatory request from the test messages
                    Packet testMandatoryRequest = testPackets.FirstOrDefault(p =>
                        p.SourceNetworkElement.TypeEnum == refNetworkElement.TypeEnum
                            && p.DestinatioNetworkElement.TypeEnum == mandatoryRequest.DestinatioNetworkElement.TypeEnum
                            && p.PacketType == mandatoryRequest.PacketType
                            && !usedTestPackets.Contains(p));

                    if (testMandatoryRequest == null)
                    {
                        message = $"Test Trace doesn’t contain a mandatory request associated with the reference trace request #{mandatoryRequest.PacketNo}";
                        LogHelper.LogError(message);
                        if (!firstErrorPacketNo.HasValue || mandatoryRequest.PacketNo < firstErrorPacketNo.Value)
                        {
                            firstErrorPacketNo = mandatoryRequest.PacketNo;
                            firstErrorMesage = message;
                        }
                        if (!_IsDebugMode) return false;
                    }
                    else
                    {
                        LogHelper.LogInfo($"Test trace contains appropriate mandatory request packet #{testMandatoryRequest.PacketNo}: {testMandatoryRequest.PacketType}");
                        usedTestPackets.Add(testMandatoryRequest);
                    }
                    
                    //Add pair of related packets
                    relatedPackets.Add(new Tuple<Packet, Packet>(mandatoryRequest, testMandatoryRequest));

                    //Get the mandatory response from the reference trace
                    var refMandatoryResponse = refPackets.FirstOrDefault(p => 
                        p.PacketNo > mandatoryRequest.PacketNo
                        && GetMandatoryResponseTypeByMandatoryRequestType(mandatoryRequest.PacketTypeEnum).Contains(p.PacketTypeEnum)
                        && p.DestinatioNetworkElement == mandatoryRequest.SourceNetworkElement
                        && p.SourceNetworkElement == mandatoryRequest.DestinatioNetworkElement
                        && p.CSeq == mandatoryRequest.CSeq
                        && !usedRefPackets.Contains(p));
                    if (refMandatoryResponse == null)
                    {
                        LogHelper.LogInfo($"Ref trace doesn't contain appropriate mandatory response for the mandatory request packet #{mandatoryRequest.PacketNo}: {mandatoryRequest.PacketType}");
                    }
                    else
                    {
                        usedRefPackets.Add(refMandatoryResponse);

                        //var ee = testPackets.Where(p =>
                        //    p == null || p.SourceNetworkElement == null || p.DestinatioNetworkElement == null).ToList();

                        //if (ee.Count > 0)
                        //{

                        //}

                        //Get a similar mandatory response from the test messages
                        Packet testMandatoryResponse = null;
                        try
                        {
                            testMandatoryResponse = testPackets.FirstOrDefault(p =>
                                p.SourceNetworkElement.TypeEnum == refMandatoryResponse.SourceNetworkElement.TypeEnum
                                && p.DestinatioNetworkElement.TypeEnum == refMandatoryResponse.DestinatioNetworkElement.TypeEnum
                                && (p.PacketType == refMandatoryResponse.PacketType || p.PacketTypeEnum == PacketType._4XX_6XX)
                                && p.CSeq == testMandatoryRequest.CSeq
                                && !usedTestPackets.Contains(p)
                                && (usedTestPackets.Count == 0 || p.PacketNo > usedTestPackets.Last().PacketNo));


                            //foreach (Packet p in testPackets)
                            //{
                            //    try
                            //    {
                            //        if (p.SourceNetworkElement.TypeEnum ==
                            //            refMandatoryResponse.SourceNetworkElement.TypeEnum
                            //            && p.DestinatioNetworkElement.TypeEnum ==
                            //            refMandatoryResponse.DestinatioNetworkElement.TypeEnum
                            //            && p.PacketType == refMandatoryResponse.PacketType
                            //            && p.PacketNo > testMandatoryRequest.PacketNo
                            //            && !usedTestPackets.Contains(p))
                            //        {
                            //            testMandatoryResponse = p;
                            //            break;
                            //        }
                            //    }
                            //    catch (Exception eeee)
                            //    {

                            //    }
                            //}
                            
                        }
                        catch (Exception exc)
                        {

                        }

                        if (testMandatoryResponse == null)
                        {
                            message = $"Test Trace doesn’t contain a mandatory response associated with the reference trace response #{refMandatoryResponse.PacketNo}";
                            LogHelper.LogError(message);
                            if (!firstErrorPacketNo.HasValue || refMandatoryResponse.PacketNo < firstErrorPacketNo.Value)
                            {
                                firstErrorPacketNo = refMandatoryResponse.PacketNo;
                                firstErrorMesage = message;
                            }
                            if (!_IsDebugMode) return false;
                        }
                        else
                        {
                            LogHelper.LogInfo($"Test trace contains appropriate mandatory response packet #{testMandatoryResponse.PacketNo}: {testMandatoryResponse.PacketType}");
                            usedTestPackets.Add(testMandatoryResponse);
                        }

                        //Add pair of related packets
                        relatedPackets.Add(new Tuple<Packet, Packet>(refMandatoryResponse, testMandatoryResponse));
                    }
                }






            }



            LogHelper.LogInfo($"Analyse Call ID successfully completed");

            return true;
        }








        public static bool IsMandatoryRequest(PacketType packetType)
        {
            return packetType == PacketType.INVITE
                   || packetType == PacketType.ACK
                   || packetType == PacketType.BYE
                   || packetType == PacketType.MESSAGE;
        }

        public static bool IsMandatoryResponse(PacketType packetType)
        {
            return packetType == PacketType._200
                   || packetType == PacketType._202
                   || packetType == PacketType._4XX_6XX;
        }

        public static bool IsMandatoryMessage(this Packet packet)
        {
            return IsMandatoryRequest(packet.PacketTypeEnum) || IsMandatoryResponse(packet.PacketTypeEnum);
        }

        
        private static List<PacketType> GetMandatoryResponseTypeByMandatoryRequestType(PacketType requestType)
        {
            if (requestType == PacketType.INVITE)
            {
                return new List<PacketType>(){ PacketType._200, PacketType._4XX_6XX };
            }

            if (requestType == PacketType.BYE)
            {
                return new List<PacketType>() { PacketType._200 };
            }

            return new List<PacketType>();
        }

        
    }
}
