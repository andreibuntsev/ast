﻿
using System;
using System.Threading.Tasks;
using System.Windows;
using AST.Common;
using AST.DataModel;
using AST.Enums;
using AST.ViewModel;

namespace AST.Logic
{
    //TODO: Make it non-static
    public static class SimpleCallScenarioProcessor
    {
        public static event EventHandler<string> StatusChanged;

        private static bool _IsInProgress = false;
        private static DeviceDataModel _DeviceA;
        private static DeviceDataModel _DeviceB;

        public static async Task Process(ScenarioControlModel scenario)
        {
            try
            {
                if (_IsInProgress) return;
                _IsInProgress = true;

                if (scenario.DeviceA == null)
                {
                    MessageBox.Show("Device A must be specified!", "Error");
                    return;
                }

                if (scenario.DeviceB == null)
                {
                    MessageBox.Show("Device B must be specified!", "Error");
                    return;
                }

                if (scenario.DeviceANetworkTypeEnum == null)
                {
                    MessageBox.Show("Device A Network Type must be specified!", "Error");
                    return;
                }

                if (scenario.DeviceBNetworkTypeEnum == null)
                {
                    MessageBox.Show("Device B Network Type must be specified!", "Error");
                    return;
                }

                _DeviceA = scenario.DeviceA;
                _DeviceB = scenario.DeviceB;

                //Turn Off the flight mode
                await SetFlightMode(scenario.DeviceA, false);
                await SetFlightMode(scenario.DeviceB, false);


                //Change Network for device A
                if (scenario.DeviceANetworkTypeEnum == DeviceNetworkType.G3)
                {
                    if (!_DeviceA.Is4GEnabled && _DeviceA.IsVoLTEEnabled)
                    {
                        //We cannot disable VoLTE in 3G mode
                        await Set4G(scenario.DeviceA);
                    }

                    await SetVoLTE(scenario.DeviceA, false);
                    await Set3G(scenario.DeviceA);
                }
                else if (scenario.DeviceANetworkTypeEnum == DeviceNetworkType.G4)
                {
                    await Set4G(scenario.DeviceA);
                    await SetVoLTE(scenario.DeviceA, false);
                }
                else if (scenario.DeviceANetworkTypeEnum == DeviceNetworkType.G4_LTE)
                {
                    await Set4G(scenario.DeviceA);
                    await SetVoLTE(scenario.DeviceA, true);
                }


                //Change Network for device B
                if (scenario.DeviceBNetworkTypeEnum == DeviceNetworkType.G3)
                {
                    if (!_DeviceB.Is4GEnabled && _DeviceB.IsVoLTEEnabled)
                    {
                        //We cannot disable VoLTE in 3G mode
                        await Set4G(scenario.DeviceB);
                    }

                    await SetVoLTE(scenario.DeviceB, false);
                    await Set3G(scenario.DeviceB);
                }
                else if (scenario.DeviceBNetworkTypeEnum == DeviceNetworkType.G4)
                {
                    await Set4G(scenario.DeviceB);
                    await SetVoLTE(scenario.DeviceB, false);
                }
                else if (scenario.DeviceBNetworkTypeEnum == DeviceNetworkType.G4_LTE)
                {
                    await Set4G(scenario.DeviceB);
                    await SetVoLTE(scenario.DeviceB, true);
                }


                //Reboot
                if (scenario.DeviceARestartBeforeTest || scenario.DeviceBRestartBeforeTest)
                {
                    if (scenario.DeviceARestartBeforeTest)
                    {
                        StatusChanged?.Invoke(scenario.DeviceA, "Rebooting Device A.");
                        await scenario.DeviceA.Reboot();
                    }

                    if (scenario.DeviceBRestartBeforeTest)
                    {
                        StatusChanged?.Invoke(scenario.DeviceB, "Rebooting Device B.");
                        await scenario.DeviceB.Reboot();
                    }

                    StatusChanged?.Invoke(null, "Rebooting will take 3 minutes.");
                    await Task.Delay(1000 * 60 * 3);
                    StatusChanged?.Invoke(null, "Rebooting completed.");

                    if (scenario.DeviceARestartBeforeTest)
                    {
                        scenario.DeviceA.SwipeRight();
                        scenario.DeviceA.SwipeRight();
                        StatusChanged?.Invoke(scenario.DeviceA, "Device A unlocking.");
                    }

                    if (scenario.DeviceBRestartBeforeTest)
                    {
                        scenario.DeviceB.SwipeRight();
                        scenario.DeviceB.SwipeRight();
                        StatusChanged?.Invoke(scenario.DeviceB, "Device B unlocking.");
                    }
                }

                //Flight Mode
                //await SetFlightMode(scenario.DeviceA, scenario.DeviceAFlightMode);
                //await SetFlightMode(scenario.DeviceB, scenario.DeviceBFlightMode);

                //Dialing
                string callingNumber = scenario.IsLocalCall
                    ? scenario.DeviceB.ASTDevice.Number.Replace("+61", "0")
                    : scenario.DeviceB.ASTDevice.Number;
                StatusChanged?.Invoke(scenario.DeviceA, "Dialing " + callingNumber);
                await scenario.DeviceA.DialNumber(callingNumber);
                StatusChanged?.Invoke(scenario.DeviceA, "Calling " + callingNumber);

                //Accept call by device B
                await AcceptIncoming();
                StatusChanged?.Invoke(scenario.DeviceA,
                    "Device B accepted the call from " + scenario.DeviceA.ASTDevice.Number);

                //Device A accepts that the call has been answered
                if (!WaitFor(() => _DeviceA.IsOutgoingCallAnswered, 10000))
                {
                    StatusChanged?.Invoke(_DeviceA,
                        "Device A cannot confirm the answered call. The Scenario has been aborted.");
                    return;
                }

                StatusChanged?.Invoke(scenario.DeviceA, "Device A confirmed the call answering.");
                StatusChanged?.Invoke(scenario.DeviceA, "The call duration will take " + scenario.SelectedDuration);

                //Wait for the call duration
                await Task.Delay(scenario.Scenario.CallDurationSeconds * 1000);
                StatusChanged?.Invoke(scenario.DeviceA, "The call will be aborted in a moment...");

                //Abort the call
                await scenario.DeviceA.AbortCall();
                StatusChanged?.Invoke(scenario.DeviceA, "The call has been aborted");

                //Scenario Completed message
                StatusChanged?.Invoke(null, "Scenario Completed");
            }
            finally
            {
                _IsInProgress = false;
            }
        }


        private static async Task Set3G(DeviceDataModel device)
        {
            int attemptNumber = 1;

            while (device.Is4GEnabled)
            {
                StatusChanged?.Invoke(device, $"Setting 3G mode for device {device.IsDeviceAOrB()}, attempt #{attemptNumber++}");
                await device.Enable3G(false);
                if (WaitFor(() => !device.Is4GEnabled, 10000))
                {
                    StatusChanged?.Invoke(device, "3G mode turned on for device " + device.IsDeviceAOrB());
                    return;
                }
            }
        }

        private static async Task Set4G(DeviceDataModel device)
        {
            int attemptNumber = 1;

            while (!device.Is4GEnabled)
            {
                StatusChanged?.Invoke(device, $"Setting 4G mode for device {device.IsDeviceAOrB()}, attempt #{attemptNumber++}");
                await device.Enable4G(false);
                if (WaitFor(() => device.Is4GEnabled, 10000))
                {
                    StatusChanged?.Invoke(device, "4G mode turned on for device " + device.IsDeviceAOrB());
                    return;
                }
            }
        }

        private static async Task SetVoLTE(DeviceDataModel device, bool enable)
        {
            int attemptNumber = 1;

            while (device.IsVoLTEEnabled != enable)
            {
                StatusChanged?.Invoke(device, $"{(enable ? "Enabling" : "Disabling")} VoLTE mode for device {device.IsDeviceAOrB()}, attempt #{attemptNumber++}");
                await device.ToggleVOLTE(false);
                if (WaitFor(() => device.IsVoLTEEnabled == enable, 10000))
                {
                    StatusChanged?.Invoke(device, $"VoLTE mode {(enable ? "Enabled" : "Disabled")} for device {device.IsDeviceAOrB()}");
                    return;
                }
            }
        }

        private static async Task SetFlightMode(DeviceDataModel device, bool enable)
        {
            int attemptNumber = 1;

            while (device.IsFlightModeEnabled != enable)
            {
                StatusChanged?.Invoke(device, $"{(enable ? "Enabling" : "Disabling")} Flight mode for device {device.IsDeviceAOrB()}, attempt #{attemptNumber++}");
                await device.ToggleFlightMode(false);
                if (WaitFor(() => device.IsFlightModeEnabled == enable, 10000))
                {
                    StatusChanged?.Invoke(device, $"Flight mode {(enable ? "Enabled" : "Disabled")} for device {device.IsDeviceAOrB()}");
                    return;
                }
            }
        }

        public static async Task AcceptIncoming()
        {
            DateTime endTime = DateTime.Now.AddSeconds(30);

            while (endTime > DateTime.Now)
            {
                if (_DeviceB.IncomingCallNumber != null && 
                    _DeviceB.IncomingCallNumber.Substring(_DeviceB.IncomingCallNumber.Length - 9, 9) == _DeviceA.ASTDevice.Number.Substring(_DeviceA.ASTDevice.Number.Length - 9, 9))
                {
                    StatusChanged?.Invoke(_DeviceB, $"Device B has incoming call from {_DeviceA.ASTDevice.Number}");
                    await _DeviceB.AcceptCall();
                    break;
                }

                await Task.Delay(2000);
            }
        }




        private static bool WaitFor(Func<bool> function, int timeout)
        {
            DateTime startTime = DateTime.Now;

            while (startTime.AddMilliseconds(timeout) > DateTime.Now)
            {
                if (function()) return true;
                Task.Delay(1000);
            }

            return false;
        }

        private static string IsDeviceAOrB(this DeviceDataModel deviceDataModel)
        {
            return deviceDataModel == _DeviceA ? "A" : "B";
        }
    }
}
