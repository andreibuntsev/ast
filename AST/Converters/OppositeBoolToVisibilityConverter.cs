#region References

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

#endregion

namespace PblMedia.Applications.Phoenix.WPFUI.Converters
{
    /// <summary>
    /// Converts opposite bool to Windows.Visibility
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class OppositeBoolToVisibilityConverter : IValueConverter
    {
        private Visibility _InvisibleValue = Visibility.Collapsed;

        /// <summary>
        /// Sets which Visibility value should be used for 'invisible' state
        /// </summary>
        public Visibility InvisibleValue
        {
            get { return _InvisibleValue; }
            set { _InvisibleValue = value; }
        }

        /// <summary>
        /// Converts boolean or nullable boolean value to visibility
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return ((bool)value) ? InvisibleValue : Visibility.Visible;
            }

            if (value is bool?)
            {
                return ((bool?)value).GetValueOrDefault(false) ? InvisibleValue : Visibility.Visible;
            }

            return Visibility.Visible;
        }

        /// <summary>
        /// Converts visibility value to boolean
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value is Visibility && (Visibility)value == InvisibleValue);
        }
    }
}
