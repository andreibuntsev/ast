﻿using AST.Common;
using System;
using System.Globalization;
using System.Windows.Data;


namespace AST.Converters
{
    /// <summary>
    /// This converter gets the description of the specified enum value
	/// </summary>
    [ValueConversion(typeof(Enum), typeof(string))]
    public sealed class EnumDescriptionConverter : IValueConverter
    {
        /// <summary>
        /// Get the enum description for the specified enum value and type
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value == null || value.ToString() == String.Empty) return null;
                return EnumHelper.GetEnumDescription((Enum)parameter ?? (Enum)value);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        /// <summary>
        /// There's no need to implement this because this converter is used for display purposes only
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
