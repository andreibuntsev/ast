using System;
using System.Windows.Data;

namespace AST.Converters
{
    /// <summary>
    /// Converter that will subtract the parameter from the value
    /// </summary>
    public class SubtractTwoNumbersConverter : IValueConverter
    {
        /// <summary>
        /// Returns (value - parameter), but only if value >= parameter
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ( !Double.IsNaN( System.Convert.ToDouble(value))  && 
                !Double.IsNaN( System.Convert.ToDouble(value)) )
            {
                  double retVal = (System.Convert.ToDouble(value) - System.Convert.ToDouble(parameter));

                  if (retVal >= 0)
                  {
                      return retVal;
                  }
            }

            return value;
        }

        /// <summary>
        /// This method is not supported.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
