﻿using AST.Common;
using AST.DataModel;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Windows;

namespace AST.DataAccess
{
    public class SqliteDataAccess
    {
        private static string _ConnectionString = ConfigurationHelper.GetConnectionString("SQLite");

        #region Scenarios

        public static List<Scenario> LoadScenarios()
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                var output = conn.Query<Scenario>("select * from Scenario", new DynamicParameters());
                return output.ToList();
            }
        }

        public static void SaveScenario(Scenario scenario)
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                if (scenario.Id.HasValue)
                {
                    //Update existent scenario
                    string cmd = $@"update Scenario set 
Name = '{scenario.Name}', 
Type = '{scenario.Type}', 
Description = '{scenario.Description}', 
DeviceANetworkType = '{scenario.DeviceANetworkType}', 
DeviceBNetworkType = '{scenario.DeviceBNetworkType}',
DeviceARestartBeforeTest = '{scenario.DeviceARestartBeforeTest}',
DeviceBRestartBeforeTest = '{scenario.DeviceBRestartBeforeTest}',
DeviceAFlightMode = '{scenario.DeviceAFlightMode}',
DeviceBFlightMode = '{scenario.DeviceBFlightMode}',
CallDurationSeconds = '{scenario.CallDurationSeconds}',
IsLocalCall = '{scenario.IsLocalCall}',
ReferenceCallFlowFileName = '{scenario.ReferenceCallFlowFileName}'
where Id = {scenario.Id.Value}";
                    conn.Execute(cmd);
                }
                else
                {
                    //Insert new scenario
                    conn.Execute(@"insert into Scenario (Name, Type, Description, DeviceANetworkType, DeviceBNetworkType, DeviceARestartBeforeTest, DeviceBRestartBeforeTest, DeviceAFlightMode, DeviceBFlightMode, CallDurationSeconds, IsLocalCall) 
values (@Name, @Type, @Description, @DeviceANetworkType, @DeviceBNetworkType, @DeviceARestartBeforeTest, @DeviceBRestartBeforeTest, @DeviceAFlightMode, @DeviceBFlightMode, @CallDurationSeconds, @IsLocalCall)", scenario);
                    //Get ID 
                    scenario.Id = conn.Query<int>("select MAX(Id) from Scenario").First();
                }
            }
        }

        public static void DeleteScenario(int scenarioId)
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                conn.Execute("delete from Packet where ScenarioId = " + scenarioId);
                conn.Execute("delete from Scenario where Id = " + scenarioId);
            }
        }

        #endregion //Scenarios


        #region Packets

        public static List<Packet> LoadPackets(int scenarioId)
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                var output = conn.Query<Packet>("select * from Packet where ScenarioId = " + scenarioId, new DynamicParameters());
                return output.ToList();
            }
        }

        public static void SavePackets(int scenarioId, IEnumerable<Packet> packets)
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                conn.Execute("delete from Packet where ScenarioId = " + scenarioId);

                if (packets != null)
                {
                    int sequence = 1;
                    foreach (Packet packet in packets.OrderBy(p => p.Sequence))
                    {
                        conn.Execute("insert into Packet (Sequence, ScenarioId, PacketNo, Time, Source, Destination, Protocol, Length, RequestType, ResponseType, CallID, CSeq) "
                            + "values (" + sequence++ + ", " + scenarioId + ", @PacketNo, @Time, @Source, @Destination, @Protocol, @Length, @RequestType, @ResponseType, @CallID, @CSeq)", packet);
                    }
                }
            }
        }

        #endregion //Packets


        #region NetworkElements

        public static List<NetworkElement> LoadNetworkElements()
        {
            try
            {
                using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
                {
                    var output = conn.Query<NetworkElement>("select * from NetworkElement", new DynamicParameters());
                    return output.ToList();
                }
            }
            catch(SQLiteException ex)
            {
                //TODO: Add logging
                //TODO: Remove MessageBox from here
                MessageBox.Show(ex.Message, "SQLiteException");
                throw ex;
            }
        }

        public static void UpdateNetworkElements(IEnumerable<NetworkElement> networkElements)
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                conn.Execute("delete from NetworkElement");
                
                if (networkElements != null)
                {
                    foreach(NetworkElement networkElement in networkElements)
                    {
                        conn.Execute("insert into NetworkElement (Type, DomainName, IPAddress) values (@Type, @DomainName, @IPAddress)", networkElement);
                    }
                }
            }
        }

        #endregion //NetworkElements


        #region Devices

        public static List<Device> LoadDevices()
        {
            try
            {
                using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
                {
                    var output = conn.Query<Device>("select * from Device", new DynamicParameters());
                    return output.ToList();
                }
            }
            catch (SQLiteException ex)
            {
                //TODO: Add logging
                //TODO: Remove MessageBox from here
                MessageBox.Show(ex.Message, "SQLiteException");
                throw ex;
            }
        }

        public static void SaveDevice(Device device)
        {
            using (IDbConnection conn = new SQLiteConnection(_ConnectionString))
            {
                if (device.Id.HasValue)
                {
                    //Update existent device
                    string cmd = $"update Device set Number = '{device.Number}' where Id = {device.Id.Value}";
                    conn.Execute(cmd);
                }
                else
                {
                    //Insert new device
                    conn.Execute("insert into Device (Serial, Model, OSVersion, Number) values (@Serial, @Model, @OSVersion, @Number)", device);
                    //Get ID 
                    device.Id = conn.Query<int>("select MAX(Id) from Device").First();
                }
            }
        }

        #endregion //NetworkElements
    }
}
