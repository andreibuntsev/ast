﻿using System.Windows;
using AST.DataModel;
using AST.ViewModel;


namespace AST.UI.Popups
{
    /// <summary>
    /// Interaction logic for EditDevicePopup.xaml
    /// </summary>
    public partial class EditDevicePopup : Window
    {
        public EditDevicePopup(Device device)
        {
            InitializeComponent();
            DataContext = new EditDevicePopupViewModel(device);
        }


        public EditDevicePopupViewModel ViewModel
        {
            get { return DataContext as EditDevicePopupViewModel; }
        }
        

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            if (!ViewModel.Number.StartsWith("+"))
            {
                MessageBox.Show("Please provide the number in international format.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ViewModel.Save();
            Close();
        }
    }
}
