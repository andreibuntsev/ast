﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using AST.Common;
using AST.DataAccess;
using AST.DataModel;
using AST.ViewModel;
using Microsoft.Win32;


namespace AST.UI.Popups
{
    /// <summary>
    /// Interaction logic for FilterPcapFilePopup.xaml
    /// </summary>
    public partial class FilterPcapFilePopup : Window
    {
        private ScenarioControlModel _ScenarioModel;

        public FilterPcapFilePopup(ScenarioControlModel scenarioModel)
        {
            InitializeComponent();
            _ScenarioModel = scenarioModel;
            DataContext = new FilterPcapFilePopupViewModel();
            ViewModel.IsFilteringEnabled = true;
            ViewModel.IsAutoAnalizing = true;
            ViewModel.IsAutoAnalizingVisible = true;

            if (scenarioModel != null && scenarioModel.Scenario != null && scenarioModel.Scenario.Id.HasValue)
            {
                var packets = SqliteDataAccess.LoadPackets(scenarioModel.Scenario.Id.Value);
                if (!packets.Any())
                {
                    ViewModel.IsAutoAnalizingVisible = false;
                    ViewModel.IsAutoAnalizing = false;
                }
            }

            ViewModel.SipFrom = "0426300897";
            ViewModel.SipTo = "0426300342";
            //ViewModel.SipFrom = "0414132575";
            //ViewModel.SipTo = "0414132448";
        }


        public FilterPcapFilePopupViewModel ViewModel
        {
            get { return DataContext as FilterPcapFilePopupViewModel; }
        }


        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonFilterFile_Click(object sender, RoutedEventArgs e)
        {
            //Open pcap file
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                List<Packet> loadedPackets = TSharkHelper.LoadPackets(dialog.FileName, ViewModel.IsFilteringEnabled ? ViewModel.FilterPcapFileCriteria : null);
                var messagesViewModel = new MessagesViewModel(_ScenarioModel, loadedPackets);
                var messagesWindow = new MessagesWindow(messagesViewModel, dialog.SafeFileName);

                Close();
                messagesWindow.Show();
                if (ViewModel.IsAutoAnalizing)
                    messagesWindow.ButtonCompare_OnClick(null, null);
            }
            else
            {
                Close();
            }
        }
    }
}
