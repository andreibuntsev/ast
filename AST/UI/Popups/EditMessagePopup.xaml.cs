﻿using System.Windows;
using AST.DataModel;
using AST.ViewModel;


namespace AST.UI.Popups
{
    /// <summary>
    /// Interaction logic for MessagesWindow.xaml
    /// </summary>
    public partial class EditMessagePopup : Window
    {
        public EditMessagePopup(MessageFlowModel messageFlowModel)
        {
            InitializeComponent();
            DataContext = new EditMessagePopupViewModel(messageFlowModel);
        }


        public EditMessagePopupViewModel ViewModel
        {
            get { return DataContext as EditMessagePopupViewModel; }
        }


        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.Save();
            Close();
        }
    }
}
