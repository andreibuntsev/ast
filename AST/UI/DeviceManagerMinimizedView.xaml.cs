﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AST.DataAccess;
using AST.DataModel;
using AST.UI.Popups;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for DeviceManagerMinimizedView.xaml
    /// </summary>
    public partial class DeviceManagerMinimizedView : UserControl
    {
        public DeviceManagerMinimizedView()
        {
            InitializeComponent();
        }


        public DeviceManagerViewModel ViewModel
        {
            get { return DataContext as DeviceManagerViewModel; }
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RefreshDeviceList();
            List<Device> savedDevices = SqliteDataAccess.LoadDevices();

            foreach (DeviceDataModel deviceDataModel in ViewModel.DeviceList)
            {
                Device device = savedDevices.FirstOrDefault(sd => sd.Serial == deviceDataModel.DeviceData.Serial);
                if (device != null)
                {
                    deviceDataModel.ASTDevice = device;
                    continue;
                }

                var newDevice = new Device()
                {
                    Serial = deviceDataModel.DeviceData.Serial,
                    Model = deviceDataModel.DeviceData.Model,
                    OSVersion = deviceDataModel.OSVersion
                };

                deviceDataModel.ASTDevice = newDevice;

                var popup = new EditDevicePopup(newDevice);
                popup.ShowDialog();
            }
        }

        private void PhoneNumber_Click(object sender, MouseButtonEventArgs e)
        {
            if (!(sender is TextBlock)) return;
            var selectedDevice = (sender as TextBlock).DataContext as DeviceDataModel;
            if (selectedDevice == null) return;

            var popup = new EditDevicePopup(selectedDevice.ASTDevice);
            popup.ShowDialog();
        }
    }
}
