﻿using AST.DataModel;
using System;
using System.Windows;
using System.Windows.Controls;
using AST.Common;
using AST.ViewModel;
using SharpAdbClient;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for DeviceManagerView.xaml
    /// </summary>
    public partial class DeviceManagerView : UserControl
    {
        public DeviceManagerView()
        {
            InitializeComponent();
        }


        public DeviceManagerViewModel ViewModel
        {
            get { return DataContext as DeviceManagerViewModel; }
        }

        private void ButtonSwipe_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) dataContext.SwipeRight();
        }

        private void ButtonReboot_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) AdbClient.Instance.Reboot(dataContext.DeviceData);
        }

        private async void Button3GOnly_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) await dataContext.Enable3G(true);
        }

        private async void Button4G_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) await dataContext.Enable4G(true);
        }

        private async void ButtonVOLTE_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) await dataContext.ToggleVOLTE(true);
        }

        private async void ButtonFlightMode_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) await dataContext.ToggleFlightMode(true);
        }

        private async void ButtonDial_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) await dataContext.DialNumber(dataContext.DialNumberString);
        }

        private async void ButtonAcceptCall_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) await dataContext.AcceptCall();
        }

        private void ButtonAbortDial_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (sender as Button).DataContext as DeviceDataModel;
            if (dataContext != null) dataContext.AbortCall();
        }
    }
}
