﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using AST.DataModel;
using AST.UI.Popups;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for MessageFlowControl.xaml
    /// </summary>
    public partial class MessageFlowControl : UserControl
    {
        const int COLUMN_WIDTH = 155;
        const string FLOW_LINE = "————————————";


        public MessageFlowControl()
        {
            InitializeComponent();
            DataContextChanged += MessageFlowControl_DataContextChanged;
        }


        public MessageFlowModel ViewModel
        {
            get { return DataContext as MessageFlowModel; }
        }


        private void MessageFlowControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ViewModel != null)
            {
                ReloadView();

                ViewModel.RefreshFlowEvent += (o, args) =>
                {
                    ReloadView();
                };

                ViewModel.PropertyChanged += (o, args) =>
                {
                    if (args.PropertyName == "IsSelected")
                    {
                        UpdateOpacity();
                    }
                };
            }
        }

        public void ReloadView()
        {
            LoadFlow();
            CreateContextMenu();
            backgroundBrush.Color = ViewModel.MessagesViewModel.CallIdBrushMapping[ViewModel.CallID].Color;
            UpdateOpacity();
        }

        public void UpdateOpacity()
        {
            if (ViewModel == null) return;
            //TODO: Move 0.35 into constant because we use it in xaml as well
            backgroundBrush.Opacity = (ViewModel.IsRelated && ViewModel.IsSelected) ? 1 : 0.35;
        }

        public void LoadFlow()
        {
            spFlow.Children.Clear();

            int columnsNumber = ViewModel.MessagesViewModel.NodeList.Count - 1;

            for (int i = 0; i < columnsNumber; i++)
            {
                if (ViewModel.Source == ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Destination == ViewModel.MessagesViewModel.NodeList[i + 1])
                {
                    //Add 'start-stop' block
                    AddBlock("|" + FLOW_LINE + ">", true);
                }
                else if (ViewModel.Source != ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Destination == ViewModel.MessagesViewModel.NodeList[i + 1]
                    && ViewModel.FlowDirection == AST.Enums.FlowDirection.Right)
                {
                    //Add '...-stop' block
                    AddBlock(FLOW_LINE + ">");
                }
                else if (ViewModel.Source == ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Destination != ViewModel.MessagesViewModel.NodeList[i + 1]
                    && ViewModel.FlowDirection == AST.Enums.FlowDirection.Right)
                {
                    //Add 'start-...' block
                    AddBlock("|" + FLOW_LINE, true);
                }
                else if (ViewModel.Source != ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Destination != ViewModel.MessagesViewModel.NodeList[i + 1]
                    && (ViewModel.MessagesViewModel.NodeList.IndexOf(ViewModel.Source) < i && ViewModel.MessagesViewModel.NodeList.IndexOf(ViewModel.Destination) > i + 1
                            || ViewModel.MessagesViewModel.NodeList.IndexOf(ViewModel.Destination) < i && ViewModel.MessagesViewModel.NodeList.IndexOf(ViewModel.Source) > i + 1))
                {
                    //Add '...-...' block
                    AddBlock(FLOW_LINE);
                }
                else if (ViewModel.Destination == ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Source == ViewModel.MessagesViewModel.NodeList[i + 1])
                {
                    //Add 'stop-start' block
                    AddBlock("<" + FLOW_LINE + "|", true);
                }
                else if (ViewModel.Destination == ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Source != ViewModel.MessagesViewModel.NodeList[i + 1]
                    && ViewModel.FlowDirection == AST.Enums.FlowDirection.Left)
                {
                    //Add 'stop-...' block
                    AddBlock("<" + FLOW_LINE);
                }
                else if (ViewModel.Destination != ViewModel.MessagesViewModel.NodeList[i] && ViewModel.Source == ViewModel.MessagesViewModel.NodeList[i + 1]
                    && ViewModel.FlowDirection == AST.Enums.FlowDirection.Left)
                {
                    //Add '...-start' block
                    AddBlock(FLOW_LINE + "|", true);
                }
                else
                {
                    //Add empty block
                    AddBlock("");
                }
            }
        }

        private void AddBlock(string text, bool addPacketType = false)
        {
            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition() {Height = new GridLength(1, GridUnitType.Star)});
            grid.RowDefinitions.Add(new RowDefinition() {Height = new GridLength(1, GridUnitType.Star)});

            var flowTextBlock = new TextBlock()
            {
                Text = text,
                Width = COLUMN_WIDTH,
                Padding = new Thickness(0, 0, 0, 0),
                VerticalAlignment = VerticalAlignment.Top,
                FontWeight = ViewModel.IsRelated ? FontWeights.ExtraBlack : FontWeights.Normal,
                Foreground = ViewModel.FlowDirection == AST.Enums.FlowDirection.Right ? Brushes.Green : Brushes.Red
            };

            if (addPacketType)
            {
                var packetTypeTextBlock = new TextBlock()
                {
                    Text = ViewModel.PacketType,
                    FontSize = 10,
                    Width = COLUMN_WIDTH,
                    TextAlignment = TextAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Bottom,
                    //Margin = new Thickness(0,2,0,0),
                    Padding = new Thickness(0,0,0,0),
                    FontWeight = ViewModel.IsRelated ? FontWeights.ExtraBlack : FontWeights.Normal,
                    Foreground = ViewModel.FlowDirection == AST.Enums.FlowDirection.Right ? Brushes.Green : Brushes.Red
                };
                grid.Children.Add(packetTypeTextBlock);
                Grid.SetRow(packetTypeTextBlock, 0);
            }
            grid.Children.Add(flowTextBlock);
            Grid.SetRow(flowTextBlock, 1);
            spFlow.Children.Add(grid);
        }


        public void CreateContextMenu()
        {
            var contextMenu = new ContextMenu();
            //contextMenu.Items.Add(new MenuItem()
            //{
            //    Header = "Delete Network Element",
            //    Command = Commands.DeleteNetworkElementCommand
            //});

            //Edit
            var editMessageButton = new Button()
            {
                Content = "Edit",
                Width = 200
            };
            editMessageButton.Click += (sender, args) =>
            {
                var editMessagePopup = new EditMessagePopup(ViewModel);
                contextMenu.IsOpen = false;
                editMessagePopup.ShowDialog();
            };
            contextMenu.Items.Add(editMessageButton);

            //Copy
            var copyMessageButton = new Button()
            {
                Content = "Copy",
                Width = 200
            };
            copyMessageButton.Click += (sender, args) =>
            {
                if (ViewModel.PacketId == 0)
                {
                    MessageBox.Show("Cannot perform operations on unsaved rows sorry mate. That thing is supposed to be fixed soon.");
                    return;
                }
                ViewModel.MessagesViewModel.CopyPacket(ViewModel.PacketId);
                contextMenu.IsOpen = false;
            };
            contextMenu.Items.Add(copyMessageButton);

            //Delete
            var deleteMessageButton = new Button()
            {
                Content = "Delete Message",
                Width = 200
            };
            deleteMessageButton.Click += (sender, args) =>
            {
                if (ViewModel.PacketId == 0)
                {
                    MessageBox.Show("Cannot perform operations on unsaved rows sorry mate. That thing is supposed to be fixed soon.");
                    return;
                }
                ViewModel.MessagesViewModel.DeletePacket(ViewModel.PacketId);
                contextMenu.IsOpen = false;
            };
            contextMenu.Items.Add(deleteMessageButton);

            //Move Up
            var moveUpMessageButton = new Button()
            {
                Content = "Move Message Up",
                Width = 200
            };
            moveUpMessageButton.Click += (sender, args) =>
            {
                if (ViewModel.PacketId == 0)
                {
                    MessageBox.Show("Cannot perform operations on unsaved rows sorry mate. That thing is supposed to be fixed soon.");
                    return;
                }
                ViewModel.MessagesViewModel.MovePacketUp(ViewModel.PacketId);
                contextMenu.IsOpen = false;
            };
            contextMenu.Items.Add(moveUpMessageButton);

            //Move Down
            var moveDownMessageButton = new Button()
            {
                Content = "Move Message Down",
                Width = 200
            };
            moveDownMessageButton.Click += (sender, args) =>
            {
                if (ViewModel.PacketId == 0)
                {
                    MessageBox.Show("Cannot perform operations on unsaved rows sorry mate. That thing is supposed to be fixed soon.");
                    return;
                }
                ViewModel.MessagesViewModel.MovePacketDown(ViewModel.PacketId);
                contextMenu.IsOpen = false;
            };
            contextMenu.Items.Add(moveDownMessageButton);

            this.ContextMenu = contextMenu;
        }

        private void TextBlockCallId_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                ViewModel.IsCallIdTextBoxMode = true;
            }
        }

        private void TextBoxCallId_OnLostFocus(object sender, RoutedEventArgs e)
        {
            ViewModel.IsCallIdTextBoxMode = false;

        }

        private void TextBoxCallId_OnMouseLeave(object sender, MouseEventArgs e)
        {
            ViewModel.IsCallIdTextBoxMode = false;
        }

        private void TextBoxCallId_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            textBoxCallId.SelectAll();
        }
    }
}
