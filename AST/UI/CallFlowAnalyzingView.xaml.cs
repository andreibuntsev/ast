﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AST.Common;
using AST.DataModel;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for CallFlowAnalyzingView.xaml
    /// </summary>
    public partial class CallFlowAnalyzingView : UserControl
    {
        private bool _RefMessagesScrolling = true;
        private bool _IsRowsLayout = true;

        public CallFlowAnalyzingView(CallFlowAnalyzingControlModel callFlowAnalyzingControlModel)
        {
            DataContext = callFlowAnalyzingControlModel;
            InitializeComponent();
        }

        public CallFlowAnalyzingControlModel ViewModel
        {
            get { return DataContext as CallFlowAnalyzingControlModel; }
        }

        private void RefScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (!_RefMessagesScrolling) return;

            var testScroller = WpfUiHelper.GetVisualChild<ScrollViewer>(testListView);

            double horizontalOffset = e.HorizontalOffset;
            if (Math.Abs(horizontalOffset) > 0.1)
            {
                testScrollViewer.ScrollToHorizontalOffset(horizontalOffset);
                return;
            }

            double verticalOffset = e.VerticalOffset;

            var refMessageFlowList = ViewModel.RefMessageFlowList.ToList();
            Tuple<Packet, Packet> relatedPacket = ViewModel.RelatedPackets.FirstOrDefault(rp => 
                refMessageFlowList.Any(m => refMessageFlowList.IndexOf(m) >= verticalOffset && rp.Item1.PacketNo == m.Packet.PacketNo));

            if (relatedPacket != null && relatedPacket.Item2 != null)
            {
                //Get the related packet index in the related scroll view
                var testMessageFlowList = ViewModel.TestMessageFlowList.ToList();
                MessageFlowModel relatedMessage = testMessageFlowList.FirstOrDefault(m => m.Packet.PacketNo == relatedPacket.Item2.PacketNo);
                if (relatedMessage == null)
                {
                    throw new InvalidOperationException("Cannot find an appropriate message in test list");
                }

                int resultOffset = testMessageFlowList.IndexOf(relatedMessage);
                if (resultOffset == -1)
                {
                    throw new InvalidOperationException("Cannot find an appropriate message in test list (1)");
                }

                testScroller.ScrollToVerticalOffset(resultOffset);
            }
        }

        private void TestScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_RefMessagesScrolling) return;

            var refScroller = WpfUiHelper.GetVisualChild<ScrollViewer>(refListView);

            double horizontalOffset = e.HorizontalOffset;
            if (Math.Abs(horizontalOffset) > 0.1)
            {
                refScrollViewer.ScrollToHorizontalOffset(horizontalOffset);
                return;
            }

            double verticalOffset = e.VerticalOffset;
            var testMessageFlowList = ViewModel.TestMessageFlowList.ToList();
            Tuple<Packet, Packet> relatedPacket = ViewModel.RelatedPackets.FirstOrDefault(rp =>
                rp.Item2 != null
                && testMessageFlowList.Any(m => testMessageFlowList.IndexOf(m) >= verticalOffset && rp.Item2.PacketNo == m.Packet.PacketNo));

            if (relatedPacket != null)
            {
                //Get the related packet index in the related scroll view
                var refMessageFlowList = ViewModel.RefMessageFlowList.ToList();
                MessageFlowModel relatedMessage = refMessageFlowList.FirstOrDefault(m => m.Packet.PacketNo == relatedPacket.Item1.PacketNo);
                if (relatedMessage == null)
                {
                    throw new InvalidOperationException("Cannot find an appropriate message in ref list");
                }

                int resultOffset = refMessageFlowList.IndexOf(relatedMessage);
                if (resultOffset == -1)
                {
                    throw new InvalidOperationException("Cannot find an appropriate message in ref list (1)");
                }

                
                refScroller.ScrollToVerticalOffset(resultOffset);
            }
        }


        private void RefListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get the selected item
            var refMessage = refListView.SelectedItem as MessageFlowModel;
            if (refMessage == null) return;

            //Get associated tuple
            Tuple<Packet, Packet> relatedPacket = ViewModel.RelatedPackets.FirstOrDefault(rp => rp.Item1.PacketNo == refMessage.Packet.PacketNo);
            if (relatedPacket == null) return;

            //Select the related packet
            dataGridRelatedPackets.SelectedItem = relatedPacket;
            dataGridRelatedPackets.ScrollIntoView(relatedPacket);
            dataGridRelatedPackets.Focus();


            //Get associated message from the test messages list
            MessageFlowModel associatedMessage = ViewModel.TestMessageFlowList.FirstOrDefault(m => relatedPacket.Item2 != null && m.PacketNo == relatedPacket.Item2.PacketNo);
            if (associatedMessage == null) return;

            ViewModel.RefMessageFlowList.Where(m => m.IsRelated && m.IsSelected).ForEach(m =>
            {
                m.IsSelected = false;
            });
            refMessage.IsSelected = true;

            if (!associatedMessage.IsSelected)
            {
                testListView.SelectedItem = associatedMessage;
            }
        }

        private void TestListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get the selected item
            var testMessage = testListView.SelectedItem as MessageFlowModel;
            if (testMessage == null) return;

            //Get associated tuple
            Tuple<Packet, Packet> relatedPacket = ViewModel.RelatedPackets.FirstOrDefault(rp => rp.Item2 != null && rp.Item2.PacketNo == testMessage.Packet.PacketNo);
            if (relatedPacket == null) return;

            //Get associated message from the ref messages list
            MessageFlowModel associatedMessage = ViewModel.RefMessageFlowList.FirstOrDefault(m => m.PacketNo == relatedPacket.Item1.PacketNo);
            if (associatedMessage == null) return;

            ViewModel.TestMessageFlowList.Where(m => m.IsRelated && m.IsSelected).ForEach(m =>
            {
                m.IsSelected = false;
            });
            testMessage.IsSelected = true;

            if (!associatedMessage.IsSelected)
            {
                refListView.SelectedItem = associatedMessage;
            }
        }

        private void RefScrollViewer_OnMouseEnter(object sender, MouseEventArgs e)
        {
            _RefMessagesScrolling = true;
        }

        private void TestScrollViewer_OnMouseEnter(object sender, MouseEventArgs e)
        {
            _RefMessagesScrolling = false;
        }

        
        private void ButtonChangeLayout_Click(object sender, RoutedEventArgs e)
        {
            if (_IsRowsLayout)
            {
                //Ref
                refGrid.SetValue(Grid.RowProperty, 1);
                refGrid.SetValue(Grid.RowSpanProperty, 2);
                refGrid.SetValue(Grid.ColumnProperty, 0);
                refGrid.SetValue(Grid.ColumnSpanProperty, 1);
                //Test
                testGrid.SetValue(Grid.RowProperty, 1);
                testGrid.SetValue(Grid.RowSpanProperty, 2);
                testGrid.SetValue(Grid.ColumnProperty, 1);
                testGrid.SetValue(Grid.ColumnSpanProperty, 1);
            }
            else
            {
                //Ref
                refGrid.SetValue(Grid.RowProperty, 1);
                refGrid.SetValue(Grid.RowSpanProperty, 1);
                refGrid.SetValue(Grid.ColumnProperty, 0);
                refGrid.SetValue(Grid.ColumnSpanProperty, 2);
                //Test
                testGrid.SetValue(Grid.RowProperty, 2);
                testGrid.SetValue(Grid.RowSpanProperty, 2);
                testGrid.SetValue(Grid.ColumnProperty, 0);
                testGrid.SetValue(Grid.ColumnSpanProperty, 2);
            }

            _IsRowsLayout = !_IsRowsLayout;
        }

        private void DataGridRelatedPackets_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var selectedPackets = dataGridRelatedPackets.SelectedItem as Tuple<Packet, Packet>;
            if (selectedPackets == null) return;

            //Select Ref message
            MessageFlowModel refMessage = ViewModel.RefMessageFlowList.FirstOrDefault(m => m.PacketNo == selectedPackets.Item1.PacketNo);
            if (refMessage == null) return;
            refListView.SelectedItem = refMessage;
            refListView.ScrollIntoView(refMessage);

            //Select Test message
            if (selectedPackets.Item2 == null) return;
            MessageFlowModel testMessage = ViewModel.TestMessageFlowList.FirstOrDefault(m => m.PacketNo == selectedPackets.Item2.PacketNo);
            if (testMessage == null) return;
            testListView.SelectedItem = testMessage;
            testListView.ScrollIntoView(testMessage);
        }
    }
}
