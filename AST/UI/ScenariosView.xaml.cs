﻿using System.Windows;
using System.Windows.Controls;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for ScenariosView.xaml
    /// </summary>
    public partial class ScenariosView : UserControl
    {
        public ScenariosView()
        {
            InitializeComponent();
        }


        public ScenariosViewModel ViewModel
        {
            get { return DataContext as ScenariosViewModel; }
        }

        private void ButtonAddNewScenario_OnClick(object sender, RoutedEventArgs e)
        {
            //Create new empty scenario
            ViewModel.AddNewScenario();
        }

        private void ButtonDeleteScenario_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(
                "Are you sure you want to delete the scenario '" + ViewModel.SelectedScenarioControlModel.Name + "'?", "Confirmation", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                //Delete the selected scenario
                ViewModel.DeleteScenario(ViewModel.SelectedScenarioControlModel);
            }
        }
    }
}
