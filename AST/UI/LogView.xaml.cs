﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for LogView.xaml
    /// </summary>
    public partial class LogView : UserControl
    {
        public LogView()
        {
            InitializeComponent();
            (dgLogEvents.Items as INotifyCollectionChanged).CollectionChanged += (sender, args) =>
            {
                if (dgLogEvents.Items.Count > 0)
                {
                    dgLogEvents.ScrollIntoView(dgLogEvents.Items[dgLogEvents.Items.Count - 1]);
                }
            };
        }


        public LogViewModel ViewModel
        {
            get { return DataContext as LogViewModel; }
        }


        private void ButtonClearLog_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.ClearLog();
        }
    }
}
