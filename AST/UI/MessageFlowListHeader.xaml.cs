﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AST.Common;
using AST.DataModel;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for MessageFlowListHeader.xaml
    /// </summary>
    public partial class MessageFlowListHeader : UserControl
    {
        const int COLUMN_WIDTH = 155;
        const string FLOW_LINE = "————————————";


        public MessageFlowListHeader()
        {
            InitializeComponent();
            DataContextChanged += MessageFlowControl_DataContextChanged;
        }


        public MessagesViewModel ViewModel
        {
            get { return DataContext as MessagesViewModel; }
        }


        private void MessageFlowControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ViewModel != null)
            {
                //ViewModel.ModelLoaded += ViewModel_ModelLoaded;
                foreach (string node in ViewModel.NodeList)
                {
                    NetworkElement networkElement = GlobalData.NetworkElements.FirstOrDefault(ne => ne.IPAddress == node);
                    //Add 'start-stop' block
                    var newNodeTextBlock = new TextBlock()
                    {
                        Text = networkElement == null ? node : networkElement.Type,
                        Tag = node,
                        Width = COLUMN_WIDTH,
                        FontWeight = FontWeights.Bold
                    };

                    AddContextMenuToNetworkElement(newNodeTextBlock);

                    spHeader.Children.Add(newNodeTextBlock);
                }
            }
        }

        private void AddContextMenuToNetworkElement(TextBlock nodeTextBlock)
        {
            var contextMenu = new ContextMenu();
            //contextMenu.Items.Add(new MenuItem()
            //{
            //    Header = "Delete Network Element",
            //    Command = Commands.DeleteNetworkElementCommand
            //});

            var deleteNetworkElementButton = new Button()
            {
                Content = "Delete Network Element"
            };
            deleteNetworkElementButton.Click += (sender, args) =>
                {
                    spHeader.Children.Remove(nodeTextBlock);
                    ViewModel.DeleteNode(nodeTextBlock.Tag.ToString());
                    contextMenu.IsOpen = false;
                };

            contextMenu.Items.Add(deleteNetworkElementButton);

            nodeTextBlock.ContextMenu = contextMenu;
        }

        //private void DeleteNetworkElementCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        //{
        //    //throw new NotImplementedException();
        //}

        //private void DeleteNetworkElementCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        //{
        //    e.CanExecute = true;
        //    e.Handled = true;
        //}
    }
}
