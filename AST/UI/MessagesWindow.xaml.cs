﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AST.Common;
using AST.DataAccess;
using AST.DataModel;
using AST.Enums;
using AST.Logic;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for MessagesWindow.xaml
    /// </summary>
    public partial class MessagesWindow : Window
    {
        private string _PcapFileName;

        public MessagesWindow(MessagesViewModel messagesViewModel, string pcapFileName)
        {
            _PcapFileName = pcapFileName;
            DataContext = messagesViewModel;
            InitializeComponent();

            //if (!File.Exists(ViewModel.TsharkFilePath))
            //{
            //    MessageBox.Show("Please specify a correct path to tshark.exe file on the Setting panel and restart application.", "Settings Error");
            //    return;
            //}

            //FillPacketList("C:\\PCAPS\\Call_A_to_B_idle.pcap");

            ViewModel.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case "MessageFlowList":
                        foreach (MessageFlowModel messageFlowModel in listViewMessages.Items)
                        {
                            messageFlowModel.RefreshFlow();
                        }
                        break;
                }
            };
        }


        public MessagesViewModel ViewModel
        {
            get { return DataContext as MessagesViewModel; }
        }

        private void ButtonSaveCallFlow_OnClick(object sender, RoutedEventArgs e)
        {
            if (!ViewModel.ScenarioControlModel.Scenario.Id.HasValue)
            {
                MessageBox.Show("Save the Scenario first", "Call Flow");
                return;
            }

            try
            {
                LogHelper.LogInfo("MessagesWindow: Started Call Flow saving for scenarioID: " + ViewModel.ScenarioControlModel.Scenario.Id);
                SqliteDataAccess.SavePackets(ViewModel.ScenarioControlModel.Scenario.Id.Value, ViewModel.PacketList);
                ViewModel.ScenarioControlModel.Scenario.ReferenceCallFlowFileName = _PcapFileName;
                SqliteDataAccess.SaveScenario(ViewModel.ScenarioControlModel.Scenario);
                LogHelper.LogInfo("MessagesWindow: Completed Call Flow saving for scenarioID: " + ViewModel.ScenarioControlModel.Scenario.Id);
                MessageBox.Show("Call Flow has been saved", "Call Flow");
            }
            catch (Exception ex)
            {
                LogHelper.LogError("MessagesWindow: Failed Call Flow saving for scenarioID: " + ViewModel.ScenarioControlModel.Scenario.Id 
                                   + ". Error: " + ex.Message);

                //TODO: Implement unhandled exception global wrapper
                throw ex;
            }
        }

        internal void ButtonCompare_OnClick(object sender, RoutedEventArgs e)
        {
            if (!ViewModel.ScenarioControlModel.Scenario.Id.HasValue)
            {
                MessageBox.Show("The Scenario must be saved before analizing", "Call Flow");
                return;
            }

            //Load packets from DB
            var refCallFlowPackets = SqliteDataAccess.LoadPackets(ViewModel.ScenarioControlModel.Scenario.Id.Value).OrderBy(p => p.Sequence).ToList();
            if (refCallFlowPackets.Count == 0)
            {
                MessageBox.Show("No Saved Call Flow Found", "Error");
                return;
            }

            try
            {
                string message;
                string firstErrorMesage;
                int? firstErrorPacketNo = null;
                var relatedPackets = new List<Tuple<Packet, Packet>>();
                bool isSuccessful = CallFlowAnalyzer.Analyze(refCallFlowPackets, ViewModel.PacketList.ToList(), relatedPackets, out message, out firstErrorPacketNo, out firstErrorMesage);
                relatedPackets = relatedPackets.OrderBy(rp => rp.Item1.PacketNo).ToList();
                relatedPackets.ForEach(rp =>
                {
                    if (rp.Item2 == null) rp.Item1.IsRedHighlighted = true;
                    else if (rp.Item2.PacketTypeEnum == PacketType._4XX_6XX)
                    {
                        rp.Item1.IsRedHighlighted = true;
                        rp.Item2.IsRedHighlighted = true;
                    }
                });

                var firstHighlightedRelatedPackets = relatedPackets.FirstOrDefault(rp => rp.Item1.IsRedHighlighted);
                if (firstHighlightedRelatedPackets != null && 
                    (!firstErrorPacketNo.HasValue || firstHighlightedRelatedPackets.Item1.PacketNo < firstErrorPacketNo.Value))
                {
                    firstErrorPacketNo = firstHighlightedRelatedPackets.Item1.PacketNo;
                    firstErrorMesage = $"Cannot find appropriate test packet for the ref packet #{firstHighlightedRelatedPackets.Item1.PacketNo}";
                }
                
                var callFlowAnalyzingControlModel = new CallFlowAnalyzingControlModel(
                    ViewModel.ScenarioControlModel, new MessagesViewModel(ViewModel.ScenarioControlModel, null), ViewModel, relatedPackets);
                CallFlowAnalyzingView analyzingControl = new CallFlowAnalyzingView(callFlowAnalyzingControlModel);

                //Open compare window
                var newTabItem = new TabItem
                {
                    Header = "Analyzing",
                    Content = analyzingControl
                };
                MessagesTabControl.Items.Add(newTabItem);
                newTabItem.IsSelected = true;

                //Save compare report to Excel
                XlsHelper.SaveAnalyzingReport(ViewModel.ScenarioControlModel.Scenario.ReferenceCallFlowFileName,
                    _PcapFileName,
                    isSuccessful,
                    firstErrorMesage ?? message);

                if (String.IsNullOrEmpty(message))
                {
                    message = "Call Flow analyze successfully completed. Reference the log for detailed information.";
                    LogHelper.LogInfo(message);
                }
                else
                {
                    message = "Call Flow analyze completed with errors. Reference the log for detailed information.";
                    LogHelper.LogInfo(message);
                }

                MessageBox.Show($"{message}", "Analyzing Completed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) ex = ex.InnerException;
                string errorMessage = $"Unhandled Exception during analyzing: {ex.Message}, Stack Trace: {ex.StackTrace}";
                LogHelper.LogError(errorMessage);
                MessageBox.Show(errorMessage, "Unexpected Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
