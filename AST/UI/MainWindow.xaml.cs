﻿using System;
using System.Linq;
using System.Windows;
using AST.ViewModel;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();

            //Close all other application windows
            Closed += (sender, args) => Application.Current.Shutdown();
        }


        public MainViewModel ViewModel
        {
            get { return DataContext as MainViewModel; }
        }

        
        
        private void ButtonSaveNetworkElements_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Do something better to propagate data from TypeEnum to Type
            ViewModel.NetworkElements.ToList().ForEach(ne => ne.TypeEnum = ne.TypeEnum);

            if (ViewModel.NetworkElements.Any(ne => String.IsNullOrEmpty(ne.Type) || String.IsNullOrEmpty(ne.DomainName) || String.IsNullOrEmpty(ne.IPAddress)))
            {
                MessageBox.Show("Invalid Network Elements data. Please ensure that either Type and Domain Name and IP Address is specified", "Validation Error");
                return;
            }

            ViewModel.SaveNetworkElements();
        }
    }
}
