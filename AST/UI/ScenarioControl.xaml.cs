﻿using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using AST.Base;
using AST.Common;
using AST.DataModel;
using AST.Enums;
using AST.Logic;
using AST.UI.Popups;
using AST.ViewModel;
using Microsoft.Win32;


namespace AST.UI
{
    /// <summary>
    /// Interaction logic for ScenarioControl.xaml
    /// </summary>
    public partial class ScenarioControl : UserControl
    {
        public ScenarioControl()
        {
            InitializeComponent();
        }


        public ScenarioControlModel ViewModel
        {
            get { return DataContext as ScenarioControlModel; }
        }



        private void ButtonSaveScenario_OnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.SaveScenario();
        }

        private void ButtonOpenCallFlow_OnClick(object sender, RoutedEventArgs e)
        {
            //Open existent call flow
            var messagesViewModel = new MessagesViewModel(ViewModel, null);
            if (messagesViewModel.PacketList.Count > 0)
            {
                var messagesWindow = new MessagesWindow(messagesViewModel, null);
                messagesWindow.Show();
            }
        }

        private void ButtonNewCallFlow_OnClick(object sender, RoutedEventArgs e)
        {
            //Open pcap file dialog
            var openPcapFilePopup = new FilterPcapFilePopup(ViewModel);
            openPcapFilePopup.ShowDialog();
        }

        private async void ButtonLaunchScenario_OnClick(object sender, RoutedEventArgs e)
        {
            //Launch scenario
            if (ViewModel.TypeEnum == ScenarioType.SIM)
            {
                SimpleCallScenarioProcessor.StatusChanged -= SimpleCallScenarioProcessorOnStatusChanged;
                SimpleCallScenarioProcessor.StatusChanged += SimpleCallScenarioProcessorOnStatusChanged;
                await SimpleCallScenarioProcessor.Process(ViewModel);

                if (checkBoxAutoRunAnalyzing.IsChecked == true)
                {
                    //AddScenarioLog("Downloading the trace file...");
                    Notifiable.QueueForBackgroundThreadStatic(() =>
                    {
                        //Thread.Sleep(5000);
                        //AddScenarioLog("The trace file has been downloaded");
                        AddScenarioLog("Analyzing the trace file...");

                        this.Dispatcher.Invoke(() =>
                        {
                            List<Packet> loadedPackets = TSharkHelper.LoadPackets(ViewModel.ScenariosViewModel.MainViewModel.PcapFilePath, 
                                new FilterPcapFileCriteria()
                                {
                                    SipFrom = ViewModel.ScenariosViewModel.MainViewModel.PhoneFrom,
                                    SipTo = ViewModel.ScenariosViewModel.MainViewModel.PhoneTo
                                });
                            var messagesViewModel = new MessagesViewModel(ViewModel, loadedPackets);
                            LogHelper.LogInfo($"Opening Messages Windoe for PcapFile '{ViewModel.ScenariosViewModel.MainViewModel.PcapFilePath}'");
                            var messagesWindow = new MessagesWindow(messagesViewModel,
                                ViewModel.ScenariosViewModel.MainViewModel.PcapFilePath.Substring(
                                    ViewModel.ScenariosViewModel.MainViewModel.PcapFilePath.LastIndexOf("\\") + 1));
                            messagesWindow.Show();
                            messagesWindow.ButtonCompare_OnClick(null, null);

                            AddScenarioLog("Analyzing completed");
                        });
                    });
                }
            }
            else
            {
                MessageBox.Show("Not implemented yet for specified scenario type", "Error");
            }
        }

        private void SimpleCallScenarioProcessorOnStatusChanged(object sender, string message)
        {
            AddScenarioLog(message);
        }

        private void AddScenarioLog(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                textBoxScenarioLog.Text += message + "\n";
                textBoxScenarioLog.ScrollToEnd();
                LogHelper.LogInfo(message);
            });
        }
    }
}
