﻿using AST.Base;
using AST.DataAccess;
using AST.DataModel;


namespace AST.ViewModel
{
    public class EditDevicePopupViewModel : Notifiable
    {
        #region Fields

        private Device _Device;

        #endregion Fields


        #region Constructors

        public EditDevicePopupViewModel(Device device)
        {
            _Device = device;
        }

        #endregion //Constructors


        #region Properties

        public string Model
        {
            get { return _Device.Model; }
            set
            {
                if (_Device.Model == value) return;
                _Device.Model = value;
                OnPropertyChanged("Model");
            }
        }

        public string OSVersion
        {
            get { return _Device.OSVersion; }
            set
            {
                if (_Device.OSVersion == value) return;
                _Device.OSVersion = value;
                OnPropertyChanged("OSVersion");
            }
        }

        public string Number
        {
            get { return _Device.Number; }
            set
            {
                if (_Device.Number == value) return;
                _Device.Number = value;
                OnPropertyChanged("Number");
            }
        }

        #endregion //Properties


        #region Methods

        public void Save()
        {
            SqliteDataAccess.SaveDevice(_Device);
        }

        #endregion //Methods
    }
}
