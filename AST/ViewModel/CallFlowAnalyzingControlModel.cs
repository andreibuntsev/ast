﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AST.Base;
using AST.Common;
using AST.DataModel;
using AST.Logic;

namespace AST.ViewModel
{
    public class CallFlowAnalyzingControlModel : Notifiable
    {
        #region Fields

        private bool _RefShowOptionalMessages;
        private bool _TestShowOptionalMessages;
        private bool _RefShowRelatedMessagesOnly;
        private bool _TestShowRelatedMessagesOnly;

        #endregion //Fields


        #region Constructors

        /// <summary>
        /// Creates a new instance of the analyzing model
        /// </summary>
        /// <param name="scenarioControlModel"></param>
        /// <param name="refMessagesViewModel"></param>
        /// <param name="testMessagesViewModel"></param>
        /// <param name="relatedPackets"></param>
        public CallFlowAnalyzingControlModel(ScenarioControlModel scenarioControlModel, MessagesViewModel refMessagesViewModel, MessagesViewModel testMessagesViewModel, List<Tuple<Packet, Packet>> relatedPackets)
        {
            ScenarioControlModel = scenarioControlModel;
            RefMessagesViewModel = refMessagesViewModel;
            TestMessagesViewModel = testMessagesViewModel;
            RelatedPackets = relatedPackets;

            //Make related messages bold
            RefMessagesViewModel.MessageFlowList.Where(m => RelatedPackets.Any(p => p.Item1.PacketNo == m.PacketNo)).ForEach(m => m.IsRelated = true);
            TestMessagesViewModel.MessageFlowList.Where(m => RelatedPackets.Any(p => p.Item2 != null && p.Item2.PacketNo == m.PacketNo)).ForEach(m => m.IsRelated = true);

            RefShowOptionalMessages = true;
            TestShowOptionalMessages = true;
            RefShowRelatedMessagesOnly = false;
            TestShowRelatedMessagesOnly = false;

            //Synchronize CallIds color mapping
            for (int i = 0; i < RefMessagesViewModel.CallIdBrushMapping.Count; i++)
            {
                if (i >= TestMessagesViewModel.CallIdBrushMapping.Count)
                {
                    break;
                }

                TestMessagesViewModel.CallIdBrushMapping[TestMessagesViewModel.CallIdBrushMapping.ToList()[i].Key] =
                    RefMessagesViewModel.CallIdBrushMapping.ToList()[i].Value;
            }
        }

        #endregion //Constructors


        #region Properties

        public ScenarioControlModel ScenarioControlModel { get; private set; }

        public MessagesViewModel RefMessagesViewModel { get; private set; }

        public MessagesViewModel TestMessagesViewModel { get; private set; }

        public List<Tuple<Packet, Packet>> RelatedPackets { get; private set; }


        public ObservableCollection<MessageFlowModel> RefMessageFlowList
        {
            get
            {
                return new ObservableCollection<MessageFlowModel>(RefMessagesViewModel.MessageFlowList.Where(m =>
                    (RefShowOptionalMessages || m.Packet.IsMandatoryMessage()) 
                    && (!RefShowRelatedMessagesOnly || m.IsRelated)));
            }
        }

        public ObservableCollection<MessageFlowModel> TestMessageFlowList
        {
            get
            {
                return new ObservableCollection<MessageFlowModel>(TestMessagesViewModel.MessageFlowList.Where(m =>
                    (TestShowOptionalMessages || m.Packet.IsMandatoryMessage())
                    && (!TestShowRelatedMessagesOnly || m.IsRelated)));
            }
        }

        
        public bool RefShowOptionalMessages
        {
            get { return _RefShowOptionalMessages; }
            set
            {
                if (_RefShowOptionalMessages == value) return;
                _RefShowOptionalMessages = value;
                OnPropertyChanged("RefShowOptionalMessages");
                OnPropertyChanged("RefMessageFlowList");
            }
        }

        public bool TestShowOptionalMessages
        {
            get { return _TestShowOptionalMessages; }
            set
            {
                if (_TestShowOptionalMessages == value) return;
                _TestShowOptionalMessages = value;
                OnPropertyChanged("TestShowOptionalMessages");
                OnPropertyChanged("TestMessageFlowList");
            }
        }

        public bool RefShowRelatedMessagesOnly
        {
            get { return _RefShowRelatedMessagesOnly; }
            set
            {
                if (_RefShowRelatedMessagesOnly == value) return;
                _RefShowRelatedMessagesOnly = value;
                OnPropertyChanged("RefShowRelatedMessagesOnly");
                OnPropertyChanged("RefMessageFlowList");
            }
        }

        public bool TestShowRelatedMessagesOnly
        {
            get { return _TestShowRelatedMessagesOnly; }
            set
            {
                if (_TestShowRelatedMessagesOnly == value) return;
                _TestShowRelatedMessagesOnly = value;
                OnPropertyChanged("TestShowRelatedMessagesOnly");
                OnPropertyChanged("TestMessageFlowList");
            }
        }

        #endregion //Properties


        #region Methods



        #endregion //Methods
    }
}
