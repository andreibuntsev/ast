﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using AST.Base;
using AST.Common;
using AST.DataModel;
using AST.Enums;


namespace AST.ViewModel
{
    public class LogViewModel : Notifiable
    {
        #region Fields

        private bool _ShowInfo;
        private bool _ShowWarnings = true;
        private bool _ShowErrors = true;

        private bool _IsAutoRefresh = true;
        private bool _IsLoggingEnabled = true;

        public object _lockableObject = new object();

        #endregion //Fields


        #region Constructors

        public LogViewModel()
        {
            LogEvents = new ObservableCollection<LogEvent>();
            BindingOperations.EnableCollectionSynchronization(LogEvents, _lockableObject);
            LogHelper.LogEventAdded += LogHelper_LogEventAdded;
        }

        #endregion //Constructors


        #region Properties

        public ObservableCollection<LogEvent> LogEvents { get; private set; }

        public ObservableCollection<LogEvent> DisplayableLogEvents
        {
            get { return new ObservableCollection<LogEvent>(LogEvents.Where(l => l.IsVisible)); }
        }

        public bool IsLoggingEnabled
        {
            get { return _IsLoggingEnabled; }
            set
            {
                if (_IsLoggingEnabled == value) return;
                _IsLoggingEnabled = value;
                if (value)
                {
                    LogHelper.LogEventAdded += LogHelper_LogEventAdded;
                }
                else
                {
                    LogHelper.LogEventAdded -= LogHelper_LogEventAdded;
                    ClearLog();
                }
                OnPropertyChanged("IsLoggingEnabled");
            }
        }

        public bool ShowInfo
        {
            get { return _ShowInfo; }
            set
            {
                if (_ShowInfo == value) return;
                _ShowInfo = value;
                OnPropertyChanged("ShowInfo");
                if (IsAutoRefresh) RefreshEventsVisibility();
            }
        }

        public bool ShowWarnings
        {
            get { return _ShowWarnings; }
            set
            {
                if (_ShowWarnings == value) return;
                _ShowWarnings = value;
                OnPropertyChanged("ShowWarnings");
                if (IsAutoRefresh) RefreshEventsVisibility();
            }
        }

        public bool ShowErrors
        {
            get { return _ShowErrors; }
            set
            {
                if (_ShowErrors == value) return;
                _ShowErrors = value;
                OnPropertyChanged("ShowErrors");
                if (IsAutoRefresh) RefreshEventsVisibility();
            }
        }

        public bool IsAutoRefresh
        {
            get { return _IsAutoRefresh; }
            set
            {
                if (_IsAutoRefresh == value) return;
                _IsAutoRefresh = value;
                OnPropertyChanged("IsAutoRefresh");
            }
        }

        #endregion //Properties


        #region Methods

        public void ClearLog()
        {
            LogEvents.Clear();
            OnPropertyChanged("DisplayableLogEvents");
        }

        private bool IsEventVisible(LogEvent logEvent)
        {
            return (logEvent.Level == LogEventLevel.Info && ShowInfo)
                   || (logEvent.Level == LogEventLevel.Warning && ShowWarnings)
                   || (logEvent.Level == LogEventLevel.Error && ShowErrors);
        }

        
        private void RefreshEventsVisibility()
        {
            Task.Factory.StartNew(() =>
            {
                lock (_lockableObject)
                {
                    foreach (LogEvent logEvent in LogEvents)
                    {
                        logEvent.IsVisible = IsEventVisible(logEvent);
                    }
                    OnPropertyChanged("DisplayableLogEvents");
                }
            });
            
        }

        private void LogHelper_LogEventAdded(object sender, LogEvent logEvent)
        {
            Task.Factory.StartNew(() =>
            {
                lock (_lockableObject)
                {
                    LogEvents.Add(logEvent);
                    logEvent.IsVisible = IsEventVisible(logEvent);
                    if (IsAutoRefresh) OnPropertyChanged("DisplayableLogEvents");
                }
            });
        }

        #endregion //Methods
    }
}
