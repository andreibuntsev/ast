﻿using AST.DataModel;
using System.Collections.Generic;
using AST.Base;
using AST.Common;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using AST.DataAccess;

namespace AST.ViewModel
{
    public class MessagesViewModel : Notifiable
    {
        #region Fields

        #endregion //Fields


        #region Constructors

        /// <summary>
        /// Open call flow from pcap file
        /// </summary>
        /// <param name="scenarioControlModel"></param>
        /// <param name="packets"></param>
        public MessagesViewModel(ScenarioControlModel scenarioControlModel, List<Packet> packets)
        {
            ScenarioControlModel = scenarioControlModel;
            PacketList = new ObservableCollection<Packet>();
            NodeList = new ObservableCollection<string>();
            MessageFlowList = new ObservableCollection<MessageFlowModel>();
            CallIdBrushMapping = new Dictionary<string, SolidColorBrush>();

            IsReferenceTrace = packets == null;
            LoadPacketList(packets);
        }

        #endregion //Constructors


        #region Properties

        public ScenarioControlModel ScenarioControlModel { get; private set; }

        public ObservableCollection<Packet> PacketList { get; private set; }

        public ObservableCollection<string> NodeList { get; private set; }

        public ObservableCollection<MessageFlowModel> MessageFlowList { get; private set; }

        public Dictionary<string, SolidColorBrush> CallIdBrushMapping { get; private set; }

        public bool IsReferenceTrace { get; private set; }

        #endregion //Properties


        #region Methods

        /// <summary>
        /// Loads packet list from DB or pcap file
        /// </summary>
        private void LoadPacketList(List<Packet> packets)
        {
            if (!ScenarioControlModel.Scenario.Id.HasValue)
            {
                MessageBox.Show("Save the Scenario first", "Call Flow");
                return;
            }

            try
            {
                LogHelper.LogInfo($"LoadPacketList: Started Loading for ScenarioID '{ScenarioControlModel.Scenario.Id}'");
                if (packets == null)
                {
                    //Load packets from DB
                    packets = SqliteDataAccess.LoadPackets(ScenarioControlModel.Scenario.Id.Value).OrderBy(p => p.Sequence).ToList();
                }

                if (packets.Count == 0)
                {
                    MessageBox.Show("No Packets Found!", "Error");
                }
                else
                {
                    foreach (Packet packet in packets)
                    {
                        PacketList.Add(packet);
                        if (!NodeList.Contains(packet.Source)) NodeList.Add(packet.Source);
                        if (!NodeList.Contains(packet.Destination)) NodeList.Add(packet.Destination);

                        MessageFlowList.Add(new MessageFlowModel(packet, this));

                        if (!CallIdBrushMapping.ContainsKey(packet.CallID))
                        {
                            //Add new CallId-Brush mapping
                            CallIdBrushMapping.Add(packet.CallID, BrushHelper.GetRandomBrush() as SolidColorBrush);
                        }
                    }

                    //Check if (nodelist.Count > 1)
                    if (NodeList.Count < 2)
                    {
                        throw new InvalidOperationException("Nodelist should have at least 2 nodes");
                    }
                }

                LogHelper.LogInfo($"LoadPacketList: Completed Loading for ScenarioID '{ScenarioControlModel.Scenario.Id}'. {PacketList.Count} packets loaded.");
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"LoadPacketList: failed Loading for ScenarioID '{ScenarioControlModel.Scenario.Id}'");
                MessageBox.Show(ex.Message, "Unhandled Error");
                throw ex;
            }
        }

        public void DeleteNode(string node)
        {
            NodeList.Remove(node);
            List<Packet> packetsToRemove = PacketList.Where(p => p.Source == node || p.Destination == node).ToList();
            packetsToRemove.ForEach(p => PacketList.Remove(p));

            List<MessageFlowModel> messagesToRemove = MessageFlowList.Where(p => p.Source == node || p.Destination == node).ToList();
            messagesToRemove.ForEach(m => MessageFlowList.Remove(m));
        }

        /// <summary>
        /// Copies Packet
        /// </summary>
        /// <param name="packetId"></param>
        public void CopyPacket(int packetId)
        {
            Packet packetToCopy = PacketList.FirstOrDefault(p => p.Id == packetId);
            if (packetToCopy != null)
            {
                //Increase sequence of all packets upward this
                PacketList.Where(p => p.Sequence >= packetToCopy.Sequence).ToList().ForEach(p => p.Sequence++);

                //Clone the packet
                var copiedPacket = packetToCopy.Clone() as Packet;
                copiedPacket.Sequence--;
                copiedPacket.Id = PacketList.Max(p => p.Id) + 1;
                PacketList.Insert(PacketList.IndexOf(packetToCopy), copiedPacket);

                //Clone the message
                MessageFlowModel messageToCopy = MessageFlowList.FirstOrDefault(m => m.PacketId == packetId);
                if (messageToCopy != null)
                {
                    var copiedMessage = new MessageFlowModel(copiedPacket, this);

                    //Increase sequence of all messages upward this
                    MessageFlowList.Where(m => m.Sequence >= messageToCopy.Sequence).ToList().ForEach(m => m.Sequence++);

                    //Insert new message
                    MessageFlowList.Insert(MessageFlowList.IndexOf(messageToCopy), copiedMessage);
                }
            }
        }

        /// <summary>
        /// Deletes Packet
        /// </summary>
        /// <param name="packetId"></param>
        public void DeletePacket(int packetId)
        {
            Packet packetToRemove = PacketList.FirstOrDefault(p => p.Id == packetId);
            if (packetToRemove != null)
            {
                PacketList.Remove(packetToRemove);
            }

            MessageFlowModel messageToRemove = MessageFlowList.FirstOrDefault(m => m.PacketId == packetId);
            if (messageToRemove != null)
            {
                MessageFlowList.Remove(messageToRemove);
            }
        }

        /// <summary>
        /// Moves packet Up, updates sequence
        /// </summary>
        /// <param name="packetId"></param>
        public void MovePacketUp(int packetId)
        {
            Packet packetToMove = PacketList.FirstOrDefault(p => p.Id == packetId);
            if (packetToMove != null)
            {
                int packetIndex = PacketList.IndexOf(packetToMove);
                if (packetIndex > 0)
                {
                    //Swap sequences the packet and previous packet
                    int prevPacketSequence = PacketList[packetIndex - 1].Sequence;
                    PacketList[packetIndex - 1].Sequence = packetToMove.Sequence;
                    packetToMove.Sequence = prevPacketSequence;
                    PacketList.Remove(packetToMove);
                    PacketList.Insert(packetIndex - 1, packetToMove);
                }
            }

            MessageFlowModel messageToMove = MessageFlowList.FirstOrDefault(m => m.PacketId == packetId);
            if (messageToMove != null)
            {
                int messageIndex = MessageFlowList.IndexOf(messageToMove);
                if (messageIndex > 0)
                {
                    //Swap sequences the message and previous message
                    int prevMessageSequence = MessageFlowList[messageIndex - 1].Sequence;
                    MessageFlowList[messageIndex - 1].Sequence = messageToMove.Sequence;
                    messageToMove.Sequence = prevMessageSequence;
                    MessageFlowList.Remove(messageToMove);
                    MessageFlowList.Insert(messageIndex - 1, messageToMove);
                }
            }
        }

        /// <summary>
        /// Moves packet Down, updates sequence
        /// </summary>
        /// <param name="packetId"></param>
        public void MovePacketDown(int packetId)
        {
            Packet packetToMove = PacketList.FirstOrDefault(p => p.Id == packetId);
            if (packetToMove != null)
            {
                int packetIndex = PacketList.IndexOf(packetToMove);
                if (packetIndex < PacketList.Count - 1)
                {
                    //Swap sequences the packet and next packet
                    int nextPacketSequence = PacketList[packetIndex + 1].Sequence;
                    PacketList[packetIndex + 1].Sequence = packetToMove.Sequence;
                    packetToMove.Sequence = nextPacketSequence;
                    PacketList.Remove(packetToMove);
                    PacketList.Insert(packetIndex + 1, packetToMove);
                }
            }

            MessageFlowModel messageToMove = MessageFlowList.FirstOrDefault(m => m.PacketId == packetId);
            if (messageToMove != null)
            {
                int messageIndex = MessageFlowList.IndexOf(messageToMove);
                if (messageIndex < MessageFlowList.Count - 1)
                {
                    //Swap sequences the message and next message
                    int nextMessageSequence = MessageFlowList[messageIndex + 1].Sequence;
                    MessageFlowList[messageIndex + 1].Sequence = messageToMove.Sequence;
                    messageToMove.Sequence = nextMessageSequence;
                    MessageFlowList.Remove(messageToMove);
                    MessageFlowList.Insert(messageIndex + 1, messageToMove);
                }
            }
        }

        #endregion //Methods
    }
}
