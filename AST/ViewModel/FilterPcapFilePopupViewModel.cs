﻿using System;
using AST.Base;
using AST.DataModel;


namespace AST.ViewModel
{
    public class FilterPcapFilePopupViewModel : Notifiable
    {
        #region Fields

        private FilterPcapFileCriteria _FilterPcapFileCriteria;

        private bool _IsFilteringEnabled;

        private bool _IsAutoAnalizing;

        private bool _IsAutoAnalizingVisible;

        #endregion //Fields


        #region Constructors

        public FilterPcapFilePopupViewModel()
        {
            _FilterPcapFileCriteria = new FilterPcapFileCriteria();
        }

        #endregion //Constructors


        #region Properties

        public FilterPcapFileCriteria FilterPcapFileCriteria
        {
            get { return _FilterPcapFileCriteria; }
        }

        public string SipFrom
        {
            get { return _FilterPcapFileCriteria.SipFrom; }
            set
            {
                if (_FilterPcapFileCriteria.SipFrom == value) return;
                _FilterPcapFileCriteria.SipFrom = value;
                OnPropertyChanged("SipFrom");
            }
        }

        public string SipTo
        {
            get { return _FilterPcapFileCriteria.SipTo; }
            set
            {
                if (_FilterPcapFileCriteria.SipTo == value) return;
                _FilterPcapFileCriteria.SipTo = value;
                OnPropertyChanged("SipTo");
            }
        }

        public DateTime? TimeFrom
        {
            get { return _FilterPcapFileCriteria.TimeFrom; }
            set
            {
                if (_FilterPcapFileCriteria.TimeFrom == value) return;
                _FilterPcapFileCriteria.TimeFrom = value;
                OnPropertyChanged("TimeFrom");
            }
        }

        public DateTime? TimeTo
        {
            get { return _FilterPcapFileCriteria.TimeTo; }
            set
            {
                if (_FilterPcapFileCriteria.TimeTo == value) return;
                _FilterPcapFileCriteria.TimeTo = value;
                OnPropertyChanged("TimeTo");
            }
        }

        public bool IsFilteringEnabled
        {
            get { return _IsFilteringEnabled; }
            set
            {
                if (_IsFilteringEnabled == value) return;
                _IsFilteringEnabled = value;
                OnPropertyChanged("IsFilteringEnabled");
            }
        }

        public bool IsAutoAnalizing
        {
            get { return _IsAutoAnalizing; }
            set
            {
                if (_IsAutoAnalizing == value) return;
                _IsAutoAnalizing = value;
                OnPropertyChanged("IsAutoAnalizing");
            }
        }

        public bool IsAutoAnalizingVisible
        {
            get { return _IsAutoAnalizingVisible; }
            set
            {
                if (_IsAutoAnalizingVisible == value) return;
                _IsAutoAnalizingVisible = value;
                OnPropertyChanged("IsAutoAnalizingVisible");
            }
        }

        #endregion //Properties


        #region Methods

        //public void FilterFile()
        //{
        //    //Tsha
        //}

        #endregion //Methods
    }
}
