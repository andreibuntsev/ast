﻿using AST.Common;
using AST.DataModel;
using SharpAdbClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using AST.Base;


namespace AST.ViewModel
{
    public class DeviceManagerViewModel : Notifiable
    {
        #region Fields

        private AdbServer _Server;

        private List<DeviceDataModel> _DeviceList = new List<DeviceDataModel>();
        private bool _IsRefreshEnabled = false;
        private bool _IsRefreshInProgress = false;

        //private Timer _DeviceStatusTimer;

        #endregion //Fields


        #region Constructors

        public DeviceManagerViewModel()
        {
            _Server = new AdbServer();
            
            try
            {
                //TODO: Remove Start Server ??
                _Server.StartServer(".\\Adb\\platform-tools\\adb.exe", false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                LogHelper.LogError("DeviceManagerViewModel Error: " + ex.Message);
            }

            //_DeviceStatusTimer = new Timer(CheckStatusCallback, null, 2000, 5000);
        }

        #endregion //Constructors


        #region Properties

        public List<DeviceDataModel> DeviceList
        {
            get { return _DeviceList; }
        }

        public bool IsAnyDeviceAttached
        {
            get
            {
                return _DeviceList.Count > 0;
            }
        }

        public string AdbServerVersion
        {
            get { return _Server.GetStatus().Version.ToString(); }
        }

        //public bool IsRefreshEnabled
        //{
        //    get { return _IsRefreshEnabled; }
        //    set
        //    {
        //        if (_IsRefreshEnabled == value) return;
        //        _IsRefreshEnabled = value;
        //        OnPropertyChanged("IsRefreshEnabled");
        //    }
        //}

        #endregion //Properties


        #region Methods

        //private void CheckStatusCallback(object obj)
        //{
        //    //if (!IsRefreshEnabled || _IsRefreshInProgress) return;

        //    try
        //    {
        //        _IsRefreshInProgress = true;
        //        _DeviceList = AdbClient.Instance.GetDevices().Select(d => new DeviceDataModel(d)).Where(d => d.IsEnabled).ToList();
        //        OnPropertyChanged("DeviceList");
        //        OnPropertyChanged("IsAnyDeviceAttached");
        //        OnPropertyChanged("AdbServerVersion");
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.LogError("DeviceManager.CheckStatusCallback Error: " + ex.Message);
        //        //throw ex;
        //    }
        //    finally
        //    {
        //        _IsRefreshInProgress = false;
        //    }




        //    //List<string> features = AdbClient.Instance.GetFeatureSet(_DeviceList[0].DeviceData);
            

        //    ////AdbClient.Instance.ExecuteRemoteCommand("logcat -t 1", _Device, receiver);

        //    //this.Dispatcher.Invoke(() =>
        //    //{
        //    //    deviceCallStatusTextBlock.Text = statusString;
        //    //    //AddLog(receiver.ToString());
        //    //});
        //}

        public void RefreshDeviceList()
        {
            if (_IsRefreshInProgress) return;

            try
            {
                _IsRefreshInProgress = true;
                _DeviceList.ForEach(d => d.Dispose());
                _DeviceList = AdbClient.Instance.GetDevices().Select(d => new DeviceDataModel(d)).Where(d => d.IsEnabled).ToList();
                OnPropertyChanged("DeviceList");
                OnPropertyChanged("IsAnyDeviceAttached");
                OnPropertyChanged("AdbServerVersion");
            }
            catch (Exception ex)
            {
                LogHelper.LogError("DeviceManager.CheckStatusCallback Error: " + ex.Message);
                //throw ex;
            }
            finally
            {
                _IsRefreshInProgress = false;
            }
        }

        #endregion //Methods


    }
}
