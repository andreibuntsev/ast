﻿using System;
using AST.DataAccess;
using System.Collections.ObjectModel;
using System.Linq;
using AST.Base;
using AST.DataModel;
using AST.Enums;

namespace AST.ViewModel
{
    public class ScenariosViewModel : Notifiable
    {
        #region Fields

        private MainViewModel _MainViewModel;
        private ScenarioControlModel _SelectedScenarioControlModel;
        private ObservableCollection<ScenarioControlModel> _ScenarioList;
        
        #endregion //Fields


        #region Constructors

        public ScenariosViewModel(MainViewModel mainViewModel)
        {
            _MainViewModel = mainViewModel;
            _ScenarioList = new ObservableCollection<ScenarioControlModel>(
                SqliteDataAccess.LoadScenarios().Select(s => new ScenarioControlModel(this, s)));

            SelectedScenarioControlModel = _ScenarioList.FirstOrDefault();
            OnPropertyChanged("ScenarioList");
        }

        #endregion //Constructors


        #region Properties

        public MainViewModel MainViewModel
        {
            get { return _MainViewModel; }
        }

        public ScenarioControlModel SelectedScenarioControlModel
        {
            get { return _SelectedScenarioControlModel; }
            set
            {
                if (_SelectedScenarioControlModel == value) return;
                _SelectedScenarioControlModel = value;
                try
                {
                    OnPropertyChanged("SelectedScenarioControlModel");
                }
                catch (Exception er)
                {

                }

                ScenarioList.ToList().ForEach(s => s.OnPropertyChanged("IsSelected"));
            }
        }

        public ObservableCollection<ScenarioControlModel> ScenarioList
        {
            get { return _ScenarioList; }
        }

        #endregion //Properties


        #region Methods

        public void AddNewScenario()
        {
            //Create new empty scenario
            var newScenario = new Scenario()
            {
                Name = "New Scenario",
                TypeEnum = ScenarioType.SIM
            };
            var newScenarioModel = new ScenarioControlModel(this, newScenario);
            newScenarioModel.IsDirty = true;

            ScenarioList.Add(newScenarioModel);
            SelectedScenarioControlModel = newScenarioModel;
        }

        public void DeleteScenario(ScenarioControlModel scenarioModel)
        {
            //TODO: Can delete all scenarios, i.e. make scenarios list empty
            SelectedScenarioControlModel = ScenarioList.FirstOrDefault(s => s != scenarioModel);
            ScenarioList.Remove(scenarioModel);
            
            if (scenarioModel.Scenario.Id.HasValue)
            {
                SqliteDataAccess.DeleteScenario(scenarioModel.Scenario.Id.Value);
            }
        }

        #endregion //Methods
    }
}
