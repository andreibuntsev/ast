﻿using AST.Common;
using AST.DataAccess;
using AST.DataModel;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using AST.Base;


namespace AST.ViewModel
{
    public class MainViewModel : Notifiable
    {
        #region Fields

        #endregion //Fields


        #region Constructors

        public MainViewModel()
        {
            DeviceManagerViewModel = new DeviceManagerViewModel();
            ScenariosViewModel = new ScenariosViewModel(this);
            LogViewModel = new LogViewModel();
            NetworkElements = new ObservableCollection<NetworkElement>(GlobalData.NetworkElements);
        }

        #endregion //Constructors


        #region Properties

        public ObservableCollection<NetworkElement> NetworkElements { get; set; }

        public DeviceManagerViewModel DeviceManagerViewModel { get; private set; }

        public ScenariosViewModel ScenariosViewModel { get; private set; }

        public LogViewModel LogViewModel { get; private set; }

        public string TsharkFilePath
        {
            get
            {
                return ConfigurationHelper.GetAppSetting("TsharkFilePath");
            }
            set
            {
                ConfigurationHelper.SetAppSetting("TsharkFilePath", value);
            }
        }

        public string PcapFilePath
        {
            get
            {
                return ConfigurationHelper.GetAppSetting("PcapFilePath");
            }
            set
            {
                ConfigurationHelper.SetAppSetting("PcapFilePath", value);
            }
        }

        public string PhoneFrom
        {
            get
            {
                return ConfigurationHelper.GetAppSetting("PhoneFrom");
            }
            set
            {
                ConfigurationHelper.SetAppSetting("PhoneFrom", value);
            }
        }

        public string PhoneTo
        {
            get
            {
                return ConfigurationHelper.GetAppSetting("PhoneTo");
            }
            set
            {
                ConfigurationHelper.SetAppSetting("PhoneTo", value);
            }
        }

        public string AppVersion
        {
            get
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                return fvi.FileVersion;
            }
        }

        public string MainWindowTitle => $"Automatic Testing System {AppVersion}";

        #endregion //Properties


        #region Methods
        
        public void SaveNetworkElements()
        {
            GlobalData.NetworkElements = NetworkElements.ToList();
        }

        #endregion //Methods
    }
}
