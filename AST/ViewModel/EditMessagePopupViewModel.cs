﻿using System.Linq;
using AST.Base;
using AST.DataModel;


namespace AST.ViewModel
{
    public class EditMessagePopupViewModel : Notifiable
    {
        #region Fields

        private MessageFlowModel _OriginalMessageFlowModel;

        private string _Source;

        private string _Destination;

        private string _Protocol;

        private string _PacketType;

        #endregion Fields


        #region Constructors

        public EditMessagePopupViewModel(MessageFlowModel messageFlowModel)
        {
            _OriginalMessageFlowModel = messageFlowModel;
            _Source = messageFlowModel.Source;
            _Destination = messageFlowModel.Destination;
            _Protocol = messageFlowModel.Protocol;
            _PacketType = messageFlowModel.PacketType;
        }

        #endregion //Constructors


        #region Properties

        public string Source
        {
            get { return _Source; }
            set
            {
                if (_Source == value) return;
                _Source = value;
                OnPropertyChanged("Source");
            }
        }

        public string Destination
        {
            get { return _Destination; }
            set
            {
                if (_Destination == value) return;
                _Destination = value;
                OnPropertyChanged("Destination");
            }
        }

        public string Protocol
        {
            get { return _Protocol; }
            set
            {
                if (_Protocol == value) return;
                _Protocol = value;
                OnPropertyChanged("Protocol");
            }
        }

        public string PacketType
        {
            get { return _PacketType; }
            set
            {
                if (_PacketType == value) return;
                _PacketType = value;
                OnPropertyChanged("PacketType");
            }
        }

        #endregion //Properties


        #region Methods

        public void Save()
        {
            _OriginalMessageFlowModel.Source = _Source;
            _OriginalMessageFlowModel.Destination = _Destination;
            _OriginalMessageFlowModel.PacketType = _PacketType;
            _OriginalMessageFlowModel.OnPropertyChanged("PacketType");//TODO: Split DataContract and ViewModel

            Packet originalPacket = _OriginalMessageFlowModel.MessagesViewModel.PacketList.FirstOrDefault(p =>
                p.Id == _OriginalMessageFlowModel.PacketId);

            originalPacket.Source = _OriginalMessageFlowModel.Source;
            originalPacket.Destination = _OriginalMessageFlowModel.Destination;
        }

        #endregion //Methods
    }
}
