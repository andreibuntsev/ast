﻿using System;
using AST.DataModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using AST.Base;
using AST.Common;
using AST.DataAccess;
using AST.Enums;


namespace AST.ViewModel
{
    public class ScenarioControlModel : Notifiable
    {
        #region Fields

        private ScenariosViewModel _ScenariosViewModel;
        private Scenario _Scenario;
        private DeviceDataModel _DeviceA;
        private DeviceDataModel _DeviceB;
        private Dictionary<string, int> _DurationMapping;
        //private string _ScenarioLog;
        private bool _IsDirty;
        
        #endregion //Fields


        #region Constructors

        public ScenarioControlModel(ScenariosViewModel scenariosViewModel, Scenario scenario)
        {
            _ScenariosViewModel = scenariosViewModel;
            _Scenario = scenario;
            _DurationMapping = new Dictionary<string, int>();
            _DurationMapping.Add("10 sec", 10);
            _DurationMapping.Add("1 min", 60);
            _DurationMapping.Add("3 mins", 60 * 3);
            _DurationMapping.Add("120 mins (LDC)", 60 * 120);
            _DurationMapping.Add("130 mins (LDC)", 60 * 130);

            ScenariosViewModel.MainViewModel.DeviceManagerViewModel.PropertyChanged += DeviceManagerViewModel_PropertyChanged;
        }

        #endregion //Constructors


        #region Properties

        public Scenario Scenario
        {
            get { return _Scenario; }
        }

        public ScenariosViewModel ScenariosViewModel
        {
            get { return _ScenariosViewModel; }
        }

        public string Name
        {
            get { return _Scenario.Name; }
            set
            {
                if (_Scenario.Name == value) return;
                _Scenario.Name = value;
                IsDirty = true;
                OnPropertyChanged("Name");
            }
        }

        public ScenarioType TypeEnum
        {
            get { return _Scenario.TypeEnum; }
            set
            {
                if (_Scenario.TypeEnum == value) return;
                _Scenario.TypeEnum = value;
                IsDirty = true;
                OnPropertyChanged("TypeEnum");
                OnPropertyChanged("IsSimpleCall");
            }
        }

        public DeviceDataModel DeviceA
        {
            get { return _DeviceA; }
            set
            {
                if (_DeviceA == value) return;
                _DeviceA = value;
                OnPropertyChanged("DeviceA");
            }
        }

        public DeviceDataModel DeviceB
        {
            get { return _DeviceB; }
            set
            {
                if (_DeviceB == value) return;
                _DeviceB = value;
                OnPropertyChanged("DeviceB");
            }
        }

        public DeviceNetworkType? DeviceANetworkTypeEnum
        {
            get { return _Scenario.DeviceANetworkTypeEnum; }
            set
            {
                if (_Scenario.DeviceANetworkTypeEnum == value) return;
                _Scenario.DeviceANetworkTypeEnum = value;
                IsDirty = true;
                OnPropertyChanged("DeviceANetworkTypeEnum");
            }
        }

        public DeviceNetworkType? DeviceBNetworkTypeEnum
        {
            get { return _Scenario.DeviceBNetworkTypeEnum; }
            set
            {
                if (_Scenario.DeviceBNetworkTypeEnum == value) return;
                _Scenario.DeviceBNetworkTypeEnum = value;
                IsDirty = true;
                OnPropertyChanged("DeviceBNetworkTypeEnum");
            }
        }

        public bool DeviceARestartBeforeTest
        {
            get { return _Scenario.DeviceARestartBeforeTest == 1; }
            set
            {
                if (DeviceARestartBeforeTest == value) return;
                _Scenario.DeviceARestartBeforeTest = value ? 1 : 0;
                IsDirty = true;
                OnPropertyChanged("DeviceARestartBeforeTest");
            }
        }

        public bool DeviceBRestartBeforeTest
        {
            get { return _Scenario.DeviceBRestartBeforeTest == 1; }
            set
            {
                if (DeviceBRestartBeforeTest == value) return;
                _Scenario.DeviceBRestartBeforeTest = value ? 1 : 0;
                IsDirty = true;
                OnPropertyChanged("DeviceBRestartBeforeTest");
            }
        }

        public bool DeviceAFlightMode
        {
            get { return _Scenario.DeviceAFlightMode == 1; }
            set
            {
                if (DeviceAFlightMode == value) return;
                _Scenario.DeviceAFlightMode = value ? 1 : 0;
                IsDirty = true;
                OnPropertyChanged("DeviceAFlightMode");
            }
        }

        public bool DeviceBFlightMode
        {
            get { return _Scenario.DeviceBFlightMode == 1; }
            set
            {
                if (DeviceBFlightMode == value) return;
                _Scenario.DeviceBFlightMode = value ? 1 : 0;
                IsDirty = true;
                OnPropertyChanged("DeviceBFlightMode");
            }
        }

        public string SelectedDuration
        {
            get { return _DurationMapping.FirstOrDefault(m => m.Value == _Scenario.CallDurationSeconds).Key; }
            set
            {
                if (SelectedDuration == value) return;
                _Scenario.CallDurationSeconds = _DurationMapping[value];
                IsDirty = true;
                OnPropertyChanged("SelectedDuration");
            }
        }

        public bool IsLocalCall
        {
            get { return _Scenario.IsLocalCall == 1; }
            set
            {
                if (IsLocalCall == value) return;
                _Scenario.IsLocalCall = value ? 1 : 0;
                IsDirty = true;
                OnPropertyChanged("IsLocalCall");
            }
        }


        public bool IsSimpleCall
        {
            get { return TypeEnum == ScenarioType.SIM; }
        }

        public List<DeviceDataModel> DeviceList
        {
            get { return ScenariosViewModel.MainViewModel.DeviceManagerViewModel.DeviceList; }
        }

        public List<string> DurationList
        {
            get { return _DurationMapping.Select(m => m.Key).ToList(); }
        }

        


        public bool IsSelected
        {
            get { return ScenariosViewModel.SelectedScenarioControlModel == this; }
        }

        //public string ScenarioLog
        //{
        //    get { return _ScenarioLog; }
        //    set
        //    {
        //        if (_ScenarioLog == value) return;
        //        _ScenarioLog = value;
        //        OnPropertyChanged("ScenarioLog");
        //    }
        //}

        public bool IsDirty
        {
            get { return _IsDirty; }
            set
            {
                if (_IsDirty == value) return;
                _IsDirty = value;
                OnPropertyChanged("IsDirty");
            }
        }

        #endregion //Properties


        #region Methods

        public void SaveScenario()
        {
            try
            {
                SqliteDataAccess.SaveScenario(Scenario);
                IsDirty = false;
            }
            catch (Exception e)
            {
                LogHelper.LogError("SaveScenario Error: " + e.Message);
                MessageBox.Show(e.Message, "Error");
            }
        }

        //public void ProcessScenario()
        //{
        //    if (TypeEnum == ScenarioType.SIM)
        //    {
        //        SimpleCallScenarioProcessor.StatusChanged += (device, message) =>
        //        {
                    
        //        };
        //        SimpleCallScenarioProcessor.Process(this);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Not implemented yet for specified scenario type", "Error");
        //    }
        //}

        private void DeviceManagerViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DeviceList")
                OnPropertyChanged("DeviceList");
        }

        #endregion //Methods
    }
}
