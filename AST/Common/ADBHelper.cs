﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.Windows;
using System.Xml;
using AST.DataModel;
using SharpAdbClient.DeviceCommands;


namespace AST.Common
{
    public static class ADBHelper
    {
        public static void ScrollUp(this DeviceDataModel device)
        {
            device.DeviceData.ExecuteShellCommand("input swipe 200 500 200 2000 100", null);
        }

        public static void SwipeRight(this DeviceDataModel device)
        {
            device.DeviceData.ExecuteShellCommand("input swipe 100 1800 1000 1800 100", null);
        }

        public static void OpenSettings(this DeviceDataModel device)
        {
            device.DeviceData.ExecuteShellCommand("am start -a android.settings.SETTINGS", null);
        }

        public static void MinimizeAllApps(this DeviceDataModel device)
        {
            device.DeviceData.ExecuteShellCommand("input keyevent 3", null);
        }

        public static void PutPlus(this DeviceDataModel device)
        {
            device.DeviceData.ExecuteShellCommand("input keyevent 81", null);
        }

        private static void TouchScreen(this DeviceDataModel device, int x, int y)
        {
            device.DeviceData.ExecuteShellCommand(String.Format("input tap {0} {1}", x, y), null);
        }


        public static void TouchAcivity(this DeviceDataModel device, TouchActivityType activityType, bool notifyAboutWrongConfiguration = true)
        {
            if (!TouchConfiguration[device.DeviceKey].ContainsKey(activityType))
            {
                if (notifyAboutWrongConfiguration)
                {
                    MessageBox.Show("The device configuration doesn't support this type of activity", "Error");
                }

                return;
            }
            TouchConfiguration[device.DeviceKey][activityType].ForEach(touch => TouchScreen(device, touch.Item1, touch.Item2));
        }




        public enum TouchActivityType
        {
            ToggleFlightMode,
            ToggleVOLTE,
            Enable4G,
            Enable3G,
            OpenDialer,
            Dial1,
            Dial2,
            Dial3,
            Dial4,
            Dial5,
            Dial6,
            Dial7,
            Dial8,
            Dial9,
            DialAsterisk,
            Dial0,
            DialHash,
            StartCall,
            AbortCall
        }


        private static Dictionary<TouchActivityType, List<Tuple<int, int>>> GetTouchActivities(string device)
        {
            var doc = new XmlDocument();
            try
            {
                doc.Load("DeviceConfiguration.xml");
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot load DeviceConfiguration.xml file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.LogError(e.Message);
                return null;
            }

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                if (node.Name == "device" && node.Attributes["type"].Value == device)
                {
                    var touchActivities = new Dictionary<TouchActivityType, List<Tuple<int, int>>>();
                    foreach (XmlNode touchActivityNode in node.FirstChild.ChildNodes)
                    {
                        var points = new List<Tuple<int, int>>();
                        foreach (XmlNode pointNode in touchActivityNode.ChildNodes)
                        {
                            points.Add(new Tuple<int, int>(int.Parse(pointNode.Attributes["x"].Value), int.Parse(pointNode.Attributes["y"].Value)));
                        }
                        touchActivities.Add(EnumHelper.GetAsEnum<TouchActivityType>(touchActivityNode.Attributes["type"].Value), points);
                    }

                    return touchActivities;
                }
            }

            //Cannot find an appropriate configuration
            string errorStr = $"Cannot find an appropriate configuration for the device {device}";
            MessageBox.Show(errorStr, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            LogHelper.LogError(errorStr);
            return null;
        }


        static ADBHelper()
        {
            //Initialize touch configuration
            var devicesConfiguration = System.Configuration.ConfigurationManager.GetSection("DevicesConfiguration") as DevicesConfigurationSection;

            if (devicesConfiguration == null)
            {
                MessageBox.Show("Cannot load DevicesConfiguration section", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.LogError("Cannot load DevicesConfiguration section");
                return;
            }

            devicesConfiguration.Devices.OfType<DeviceConfigElement>().ForEach(device =>
            {
                var touchActivities = new Dictionary<TouchActivityType, List<Tuple<int, int>>>();

                device.TouchActivities.OfType<TouchActivityConfigElement>().ForEach(touchActivity =>
                {
                    var points = touchActivity.Points.OfType<PointConfigElement>().AsEnumerable().OrderBy(p => p.Id)
                        .Select(p => new Tuple<int, int>(p.X, p.Y)).ToList();

                    touchActivities.Add(touchActivity.Type, points);
                });

                TouchConfiguration.Add(device.Type, touchActivities);
            });
        }




        public static bool IsConfigurationAvailable(this DeviceDataModel device)
        {
            return TouchConfiguration.ContainsKey(device.DeviceKey);
        }


        private static readonly Dictionary<string, Dictionary<TouchActivityType, List<Tuple<int, int>>>>
            TouchConfiguration = new Dictionary<string, Dictionary<TouchActivityType, List<Tuple<int, int>>>>();
    }
}