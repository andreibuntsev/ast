﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;


namespace AST.Common
{
    /// <summary>
    /// Helper class to work with WPF UI (content controls, etc)
    /// </summary>
    public static class WpfUiHelper
    {
        private const double STANDARD_DPI = 96;

        /// <summary>
        /// Finds child element by name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public static FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);

            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop();
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int i = 0; i < count; ++i)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, i);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }

            return null;
        }

        public static T FindVisualChild<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        return (T)child;
                    }

                    T childItem = FindVisualChild<T>(child);
                    if (childItem != null) return childItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets prefix tree enumeration of visuals
        /// including specifired visual
        /// </summary>
        /// <param name="visual">Visual to be represented as tree</param>
        /// <returns>Enumaretion of all visuals in visual tree</returns>
        public static IEnumerable<Visual> ToVisualTree(this Visual visual)
        {
            yield return visual;

            if (visual == null) yield break;

            int numVisuals = VisualTreeHelper.GetChildrenCount(visual);
            for (int i = 0; i < numVisuals; ++i)
            {
                var child = (Visual)VisualTreeHelper.GetChild(visual, i);

                if (child != null)
                {
                    foreach (var subItem in child.ToVisualTree())
                    {
                        yield return subItem;
                    }
                }
            }
        }

        /// <summary>
        /// Gets first of parents which can be casted to T
        /// </summary>
        /// <typeparam name="T">Type of parent to be found</typeparam>
        /// <param name="visual">Starting visual</param>
        /// <returns>Reference to parent if it was found, null othervice</returns>
        public static T GetParentByType<T>(this Visual visual)
            where T : class
        {
            Visual parent = VisualTreeHelper.GetParent(visual) as Visual;
            if (parent == null) return null;
            if (parent is T) return parent as T;
            return GetParentByType<T>(parent);
        }

        /// <summary>
        /// Gets parent hierarchy
        ///  </summary>
        /// <param name="dependencyObject"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetParentHierarchy(DependencyObject dependencyObject)
        {
            for (DependencyObject current = dependencyObject; current != null; current = VisualTreeHelper.GetParent(current))
            {
                yield return current;
            }
        }

        /// <summary>
        /// Returns the first visual child from parent based on T
        /// </summary>        
        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                var visual = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = visual as T ?? GetVisualChild<T>(visual);

                if (child != null)
                {
                    break;
                }
            }

            return child;
        }

        #region find parent

        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child">A direct or indirect child of the queried item.</param>
        /// <returns>The first parent item that matches the submitted type parameter. If not matching item can be found, a null reference is being returned.</returns>
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = GetParentObject(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            var parent = parentObject as T;

            if (parent != null)
            {
                return parent;
            }

            //use recursion to proceed with next level
            return FindParent<T>(parentObject);

        }


        /// <summary>
        /// This method is an alternative to WPF's <see cref="VisualTreeHelper.GetParent"/> method, which also
        /// supports content elements. Do note, that for content element, this method falls back to the logical tree of the element.
        /// </summary>
        /// <param name="child">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise null.</returns>
        public static DependencyObject GetParentObject(DependencyObject child)
        {
            if (child == null) return null;
            var contentElement = child as ContentElement;

            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null) return parent;

                var fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            //if it's not a ContentElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }

        #endregion

        /// <summary>
        /// Tries to locate a given item within the visual tree, starting with the dependency object at a given position.
        /// </summary>
        /// <typeparam name="T">The type of the element to be found on the visual tree of the element at the given location.</typeparam>
        /// <param name="reference">The main element which is used to perform hit testing.</param>
        /// <param name="point">The position to be evaluated on the origin.</param>
        public static T FindByPoint<T>(UIElement reference, Point point) where T : DependencyObject
        {
            var element = reference.InputHitTest(point) as DependencyObject;

            if (element == null) return null;

            if (element is T) return (T)element;

            return FindParent<T>(element);
        }

        

        /// <summary>
        /// Gets bitmap from resource
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        public static BitmapImage GetBitmapFromResource(string resourceName)
        {
            Assembly assembly = typeof(WpfUiHelper).Assembly;

            string assemblyName = assembly.GetName().Name;
            string fullResourceName = assemblyName + "." + resourceName;

            using (Stream manifestStream = assembly.GetManifestResourceStream(fullResourceName))
            {
                var newBitmapImage = new BitmapImage();
                newBitmapImage.BeginInit();
                newBitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                newBitmapImage.CreateOptions = BitmapCreateOptions.None;

                newBitmapImage.StreamSource = manifestStream;
                newBitmapImage.EndInit();

                return newBitmapImage;
            }
        }

        /// <summary>
        /// Renders specified UI element to a bitmap
        /// </summary>
        /// <returns></returns>
        public static BitmapImage RenderToBmp(this UIElement source, double targetWidth, double targetHeight, double dpi)
        {
            double actualHeight = source.RenderSize.Height;
            double actualWidth = source.RenderSize.Width;

            double xScale = targetWidth / actualWidth;
            double yScale = targetHeight / actualHeight;

            double dpiScale = dpi / STANDARD_DPI;

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)(targetWidth * dpiScale), (int)(targetHeight * dpiScale), dpi, dpi, PixelFormats.Pbgra32);
            VisualBrush sourceBrush = new VisualBrush(source);

            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();

            using (drawingContext)
            {
                drawingContext.PushTransform(new ScaleTransform(xScale, yScale));
                drawingContext.DrawRectangle(Brushes.White, null, new Rect(new Point(0, 0), new Point(actualWidth, actualHeight)));
                drawingContext.DrawRectangle(sourceBrush, null, new Rect(new Point(0, 0), new Point(actualWidth, actualHeight)));
            }
            renderTarget.Render(drawingVisual);

            BmpBitmapEncoder bmpEncoder = new BmpBitmapEncoder();
            bmpEncoder.Frames.Add(BitmapFrame.Create(renderTarget));

            using (MemoryStream outputStream = new MemoryStream())
            {
                bmpEncoder.Save(outputStream);

                var newBitmapImage = new BitmapImage();
                newBitmapImage.BeginInit();
                newBitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                newBitmapImage.CreateOptions = BitmapCreateOptions.None;

                outputStream.Seek(0, SeekOrigin.Begin);

                newBitmapImage.StreamSource = outputStream;
                newBitmapImage.EndInit();

                return newBitmapImage;
            }
        }

        /// <summary>
        /// Renders specified UI element to a bitmap
        /// </summary>
        /// <returns></returns>
        public static BitmapImage RenderToBmp(this UIElement source, double targetWidth, double targetHeight)
        {
            return RenderToBmp(source, targetWidth, targetHeight, STANDARD_DPI);
        }

        /// <summary>
        /// Renders specified UI element to a bitmap using actual size
        /// </summary>
        /// <returns></returns>
        public static BitmapImage RenderToBmp(this UIElement source)
        {
            double actualHeight = source.RenderSize.Height;
            double actualWidth = source.RenderSize.Width;

            return RenderToBmp(source, actualWidth, actualHeight);
        }

        /// <summary>
        /// Clones a control
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="control"></param>
        /// <returns></returns>
        public static T Clone<T>(this Visual control)
        {
            string xaml = XamlWriter.Save(control);
            using (StringReader stringReader = new StringReader(xaml))
            {
                using (XmlReader xmlReader = XmlReader.Create(stringReader))
                {
                    T clonedControl = (T)XamlReader.Load(xmlReader);
                    return clonedControl;
                }
            }
        }
    }
}
