﻿using System;
using System.Collections.Generic;
using System.Windows.Media;


namespace AST.Common
{
    /// <summary>
    /// Helper methods dealing with brushes
    /// </summary>
    public static class BrushHelper
    {
        /// <summary>
        /// Holds random brushes list
        /// </summary>
        private static List<Brush> _RandomBrushesList;

        /// <summary>
        /// Random being used for pick a brush
        /// </summary>
        private static Random _RandomForBrushes = new Random();


        static BrushHelper()
        {
            InitBrushes();
        }



        /// <summary>
        /// Gets random brush
        /// </summary>
        /// <returns></returns>
        public static Brush GetRandomBrush()
        {
            return _RandomBrushesList[_RandomForBrushes.Next(_RandomBrushesList.Count)];
        }


        /// <summary>
        /// Inits random brushes
        /// </summary>
        private static void InitBrushes()
        {
            _RandomBrushesList = new List<Brush>()
            {
                Brushes.Pink,
                Brushes.DarkOrange,
                Brushes.Yellow,
                Brushes.GreenYellow,
                Brushes.YellowGreen,
                Brushes.DodgerBlue,
                Brushes.OrangeRed,
                Brushes.LawnGreen,
                Brushes.DeepSkyBlue,
                Brushes.LightBlue,
                Brushes.LightCoral,
                Brushes.LightGoldenrodYellow,
                Brushes.LightSalmon,
                Brushes.PaleVioletRed,
                Brushes.MediumSlateBlue
            };
        }
    }
}
