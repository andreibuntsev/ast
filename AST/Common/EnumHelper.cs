﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;


namespace AST.Common
{
    /// <summary>
    /// Helper methods dealing with enums
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// Represents an Enum value and description
        /// </summary>
        public class EnumData
        {
            public EnumData(string value, string description)
            {
                _Value = value;
                _Description = description;
            }

            private readonly string _Value;
            private readonly string _Description;

            public string Value
            {
                get { return _Value; }
            }

            public string Description
            {
                get { return _Description; }
            }
        }

        /// <summary>
        /// Retrieves a list of key/value pairs with enum values
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<EnumData> GetEnumData(Type type)
        {
            try
            {
                FieldInfo[] fis = type.GetFields();

                var l = new List<EnumData>();

                foreach (FieldInfo fi in fis)
                {
                    if (fi.IsSpecialName) continue;

                    var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (attributes.Length > 0)
                    {
                        l.Add(new EnumData(fi.GetValue(0).ToString(), attributes[0].Description));
                    }
                    else
                    {
                        l.Add(new EnumData(fi.Name, fi.Name));
                    }
                }
                return l;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Enumeration does not exist with the type: " + type != null ? type.Name : "null", ex);

            }

        }

        /// <summary>
        /// Returns the description for an enum or the enum itself if it has no description
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum value)
        {
            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
            }

            return null;
        }

                /// <summary>
        /// Gets an enum by description
        /// </summary>
        /// <param name="type"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static object GetEnum(Type type, string description)
        {
            FieldInfo[] fis = type.GetFields();

            foreach (FieldInfo fi in fis)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0 && attributes[0].Description == description)
                {
                    return Enum.Parse(type, fi.Name);
                }
            }

            throw new Exception("Enumeration does not exist with the description: " + description != null ? description : "null");
        }

        /// <summary>
        /// Tries to parse an Enum - works with nullable enums as well 
        /// http://www.objectreference.net/post/Enum-TryParse-Extension-Method.aspx
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valueToParse"></param>
        /// <param name="returnValue"></param>
        /// <returns></returns>
        public static bool TryParse<T>(string valueToParse, out T returnValue)
        {
            returnValue = default(T);

            Type thisType = typeof(T);
            if (thisType.IsGenericType &&
                thisType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                NullableConverter nc = new NullableConverter(typeof(T));
                thisType = nc.UnderlyingType;
            }

            if (Enum.IsDefined(thisType, valueToParse))
            {
                TypeConverter converter = TypeDescriptor.GetConverter(thisType);
                returnValue = (T)converter.ConvertFromString(valueToParse);
                return true;
            }

            return false;
        }


        /// <summary>
        /// Tries convert value to specified enum type, supports nulls, integers and strings
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T GetAsEnum<T>(object value)
        {
            if (value == DBNull.Value || value == null)
            {
                return default(T);
            }

            int intValue;
            string stringValue = value.ToString();

            if (Int32.TryParse(stringValue, out intValue))
            {
                return (T)((object)intValue);
            }

            return (T)Enum.Parse(typeof(T), stringValue);
        }
        
        /// <summary>
        /// Gets a list of enum descriptions
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<string> GetEnumDescriptionList(Type type)
        {
            var descriptionList = new List<string>();

            FieldInfo[] fis = type.GetFields();

            foreach (FieldInfo fi in fis)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                    descriptionList.Add(attributes[0].Description);
            }

            return descriptionList;
        }
    }
}
