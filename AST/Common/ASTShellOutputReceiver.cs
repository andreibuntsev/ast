﻿using System;
using SharpAdbClient;

namespace AST.Common
{
    public class ASTShellOutputReceiver : IShellOutputReceiver
    {
        public event EventHandler<string> OutputAdded;

        public DateTime RequestStarTime { get; set; }

        public bool ParsesErrors
        {
            get
            {
                //TODO: Log here?
                throw new NotImplementedException();
            }
        }

        public void AddOutput(string line)
        {
            if (OutputAdded != null) OutputAdded(this, line);
        }

        public void Flush()
        {
        }
    }
}
