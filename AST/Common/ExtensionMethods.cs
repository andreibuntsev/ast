using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;



//Dedicated ExtensionMethods Namespace to 
//ensure no Scope collision and ambiguous references.

namespace AST.Common
{
	/// <summary>
	/// All extension methods live in here
	/// </summary>
	public static class ExtensionMethods
	{
		/// <summary>
		/// Order's the collection and can be casted to List
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <param name="property"></param>
		/// <param name="listSortDirection"></param>
		/// <returns></returns>
		public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> list, string property, ListSortDirection listSortDirection)
		{
			if (string.IsNullOrEmpty(property))
			{
				throw new ArgumentException("Parameter Property cannot be blank or null");
			}


			PropertyInfo prop = typeof(T).GetProperty(property);

			if (prop == null)
			{
				throw new ArgumentException("No property '" + property + "' in + " + typeof(T).Name + "'");
			}

			if (listSortDirection == ListSortDirection.Descending)
			{
				List<T> returnList = list.OrderByDescending(x => prop.GetValue(x, null)).ToList();
				return returnList;
			}
			else
			{
				List<T> returnList = list.OrderBy(x => prop.GetValue(x, null)).ToList();
				return returnList;
			}
		}

        /// <summary>
        /// Adds the item if it doesn't already exist on the list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="item"></param>
	    public static void AddIfNotExists<T>(this IList<T> enumerable, T item)
	    {
            if (!enumerable.Contains(item)) enumerable.Add(item);
	    }

	    /// <summary>
	    /// Performs the specified action on each element of the <see cref="T:System.Collections.Generic.IEnumerable`1"/>.
	    /// </summary>
	    /// <param name="collection">A <see cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/> collection.</param>
	    /// <param name="action">The <see cref="T:System.Action`1"/> delegate to perform on each element of the <see cref="T:System.Collections.Generic.IEnumerable`1"/>.</param>
	    public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
	    {
	        foreach (T item in collection)
	        {
	            action(item);
	        }
	    }

	    /// <summary>
        /// Returns the previous element or default (NULL)
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="collection"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static TSource PreviousOrDefault<TSource>(this IEnumerable<TSource> collection, TSource item)
        {
            if (collection == null || collection.Count() < 2 || item == null)
            {
                return default(TSource);
            }

            var list = collection.ToList();
            int itemIndex = list.IndexOf(item);

            if (itemIndex <= 0)
            {
                return default(TSource);
            }
            
            return list[itemIndex - 1];
        }

        /// <summary>
        /// Returns the next element or default (NULL)
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="collection"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static TSource NextOrDefault<TSource>(this IEnumerable<TSource> collection, TSource item)
        {
            if (collection == null || collection.Count() < 2 || item == null)
            {
                return default(TSource);
            }

            var list = collection.ToList();
            int itemIndex = list.IndexOf(item);

            if (itemIndex < 0 || itemIndex == list.Count - 1)
            {
                return default(TSource);
            }

            return list[itemIndex + 1];
        }

        /// <summary>
        /// Performs the specified action on each element of the <see cref="T:System.Collections.IEnumerable"/>.
        /// </summary>
        /// <param name="collection">A <see cref="System.Collections.IEnumerable"/> collection.</param>
        /// <param name="action">The <see cref="T:System.Action`1"/> delegate to perform on each element of the <see cref="T:System.Collections.IEnumerable"/>.</param>
        public static void ForEach(this IEnumerable collection, Action<object> action)
        {
            foreach (object item in collection)
            {
                action(item);
            }
        }

        /// <summary>
        /// Performs the specified action on each element of the <see cref="T:System.Collections.Generic.IEnumerable`1"/>.
        /// </summary>
        /// <param name="collection">A <see cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/> collection.</param>
        /// <param name="action">The <see cref="T:System.Action`1"/> delegate to perform on each element of the <see cref="T:System.Collections.Generic.IEnumerable`1"/>.</param>
        /// <param name="exceptionHandler">The <see cref="T:System.Action`1"/> delegate to perform when an exception occurs whilst applying <paramref name="action"/> to an element of the collection.</param>
        /// <remarks>Any exception raised by <paramref name="exceptionHandler"/> is considered a fatal error in this scope and is allowed to propagate immediately.</remarks>
        public static void SafeForEach<T>(this IEnumerable<T> collection, Action<T> action, Action<T, Exception> exceptionHandler)
        {
            foreach (T item in collection)
            {
                try
                {
                    action(item);
                }
                catch(Exception exception)
                {
                    exceptionHandler(item, exception);
                }
            }
        }

	    /// <summary>
        /// Clears an collection and fills it by new set of values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="values"></param>
        public static void Remake<T>(this ICollection<T> collection, IEnumerable values)
        {
            collection.Clear();
            foreach (T value in values)
            {
                collection.Add(value);
            }
        }

        /// <summary>
        /// Removes all items from an collection, which doesn't belongs to enumerable
        /// and adds all items which belongs to enumerable, but doesn't belongs to collection
        /// This method could be usefull if collection is bound to an UI element
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="values"></param>
        public static void MergeWith<T>(this ICollection<T> collection, IEnumerable<T> values)
        {
            foreach (T value in values)
            {
                if (!collection.Contains(value)) collection.Add(value);
            }

            var valuesToRemove = new List<T>(collection.Where(x => !values.Contains(x)));

            foreach (T item in valuesToRemove)
            {
                collection.Remove(item);
            }
        }

        /// <summary>
        /// Converts collection to CSV string.  This method will ignore nulls and empty strings.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static string AsCsvString<T>(this IEnumerable<T> collection)
        {
            return collection.AsCsvString(",", true);
        }

        /// <summary>
        /// Converts collection to CSV string using specified separator.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">collection of objects to create the csv</param>
        /// <param name="separator">delimter between values</param>
        /// <param name="ignoreEmptyString">Whether or not to ignore empty string values</param>
        /// <returns></returns>
        public static string AsCsvString<T>(this IEnumerable<T> collection, string separator, bool ignoreEmptyString)
        {
            var sb = new StringBuilder();

            foreach (T item in collection)
            {
                if (item == null || item.ToString() == null || (ignoreEmptyString && item.ToString() == string.Empty))
                {
                    continue;
                }

                sb.AppendFormat("{0}{1}", item, separator);
            }

            string s = sb.ToString();

            if (s == null || (ignoreEmptyString && s == string.Empty))
            {
                return null;
            }

            //remove last comma
            return s.Substring(0, s.Length - separator.Length);
        }

	    /// <summary>
		/// Extension method to provide Exists method to SqlErrorCollection
		/// </summary>
		/// <param name="errorCollection"></param>
		/// <param name="match"></param>
		/// <returns></returns>
		public static bool Exist(this SqlErrorCollection errorCollection, Predicate<SqlError> match)
		{
			var errorList = new List<SqlError>(errorCollection.Count);

			foreach (SqlError error in errorCollection)
			{
				errorList.Add(error);
			}

			return errorList.Exists(match);
		}

        /// <summary>
        /// Determines if collection contains item, for which predicate is true
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool Exist<T>(this IEnumerable<T> collection, Func<T,bool> predicate)
        {
            foreach (T item in collection)
            {
                if(predicate(item))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Adds the specified item only if it's value is not <see langword="null"/>. Facilitates a more terse single-expression syntax for the caller.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="item"></param>
        public static void AddIfNotNull<T>(this ICollection<T> collection, T item) where T : class
        {
            if (item != null)
            {
                collection.Add(item);
            }
        }

	    /// <summary>
		/// Removes all line breaks from string
		/// </summary>
		/// <param name="lines"></param>
		/// <returns></returns>
		public static string RemoveLineBreaks(this string lines)
		{
            if (lines == null) return null;
            
			return lines.Replace("\r", "").Replace("\n", "");
		}

		/// <summary>
		/// Replaces all line breaks with specified string
		/// </summary>
		/// <param name="lines"></param>
		/// <param name="replacement"></param>
		/// <returns></returns>
		public static string ReplaceLineBreaks(this string lines, string replacement)
		{
            if (lines == null) return null;

			return lines.Replace("\r\n", replacement)
						.Replace("\r", replacement)
						.Replace("\n", replacement);
		}

        /// <summary>
        /// Concatinates strings from enumerable and inserts between each pair some delimiter
        /// </summary>
        /// <param name="strings">Strings to be concatinated</param>
        /// <param name="delimiter">Delimiter to be inserted between each pair</param>
        /// <returns></returns>
        public static string ConcatWithDelimiter(this IEnumerable<string> strings, string delimiter)
        {
            var sb = new StringBuilder();
            bool isFirstItem = true;
            foreach (string s in strings)
            {
                if (isFirstItem)
                {
                    isFirstItem = false;
                }
                else
                {
                    sb.Append(delimiter);
                }
                sb.Append(s);
            }
            return sb.ToString();
        }

	    /// <summary>
        /// Finds position of element by filter
        /// </summary>
        /// <typeparam name="T">Type of elements in enumerable</typeparam>
        /// <param name="enumerable">Enumarable to look for element</param>
        /// <param name="filterFunc">Filter function</param>
        /// <returns>Position of element if succeeded, -1 othervice</returns>
        public static int PositionOf<T>(this IEnumerable<T> enumerable, Func<T, bool> filterFunc)
        {
            IEnumerator<T> en = enumerable.GetEnumerator();
            for (int i = 0; en.MoveNext(); ++i)
            {
                if (filterFunc(en.Current)) return i;
            }
            return -1;
        }

        /// <summary>
        /// Finds position of specifired element
        /// </summary>
        /// <typeparam name="T">Type of elements in enumerable</typeparam>
        /// <param name="enumerable">Enumerable to look for element</param>
        /// <param name="value">Element to looking for</param>
        /// <returns>Position of element if succeeded, -1 otherwise</returns>
        public static int PositionOf<T>(this IEnumerable<T> enumerable, T value)
        {
            return PositionOf(enumerable, x => Equals(x, value));
        }

        /// <summary>
        /// Truncates a string to the maxLength.  If the value is null or less then the max length then the input value is returned.
        /// </summary>
        /// <param name="value">The string to truncate.</param>
        /// <param name="maxLength">The maximum length of the string</param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxLength)
        {
            if (value == null)
            {
                return value;
            }
            return value.Length > maxLength ? value.Substring(0, maxLength) : value;
        }

        /// <summary>
        /// Truncates a string to the maxLength from the left .  If the value is null or less then the max length then the input value is returned.
        /// </summary>
        /// <param name="value">The string to truncate.</param>
        /// <param name="maxLength">The maximum length of the string</param>
        /// <returns></returns>
        public static string TruncateLeft(this string value, int maxLength)
        {
            if (value == null)
            {
                return value;
            }
            return value.Length > maxLength ? value.Substring(value.Length - maxLength) : value;
        }

        /// <summary>
        /// Encodes string in Windows-1252 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToEncodedString(this string value)
        {
            if (value == null) return null;

            byte[] encodedChars = new UTF8Encoding().GetBytes(value);
            return Encoding.GetEncoding(1252).GetString(encodedChars);
        }

	    /// <summary>
        /// Converts a boolean to a string representation of a integer.  true = "1", false = "0"
        /// </summary>
        public static string ToIntString(this bool value)
        {
            return value ? "1" : "0";
        }

        /// <summary>
        /// Converts a specified string to text-reader
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static TextReader ToTextReader(this string str)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.AutoFlush = true;
            writer.Write(str);
            
            stream.Seek(0, SeekOrigin.Begin);
            return new StreamReader(stream);

        }

        /// <summary>
        /// Tests the associated string value to which this method acts as an extension, and if the supplied value
        /// is empty, then a null value is returned. This is used in data binding model scenarios where a null value
        /// must replace an empty string value for correct behaviour in stored procedures that exclude certain criteria
        /// based on nullness.
        /// </summary>
        /// <example>
        /// <code>
        /// if (DataContract.Title != value)
        /// {
        ///     DataContract.SupplierName = value.ToNullIfEmpty();
        ///     SendPropertyChanged("Title");
        /// }
        /// </code>
        /// Now replaces these constructs:
        /// <code>
        /// if (DataContract.Title != value)
        /// {
        ///     if (value == "")
        ///     {
        ///         value = null;
        ///     }
        ///     DataContract.Title = value;
        ///     SendPropertyChanged("Title");
        /// }
        /// </code>
        /// </example>
        /// <param name="val">A <see cref="System.String"/> containing the value to test.</param>
        /// <returns>
        /// Null if the supplied value is an empty string, otherwise the original string reference is returned.
        /// </returns>
        public static string ToNullIfEmpty(this string val)
        {
            return (String.IsNullOrEmpty(val) ? null : val);
        }

        /// <summary>
        /// Enumerates the <see cref="SqlErrorCollection"/> in the supplied <see cref="SqlException"/> and if user-defined error
        /// (error number 50000) is encountered, then a new exception is created of the parameterised type <typeparamref name="TException"/>
        /// and populated with the error message content of the user-defined SQL error. This can then be rethrown from the call site.
        /// </summary>
        /// <typeparam name="TException">The exception type to be created, populated and returned if a <see cref="SqlError"/> matching error number 5000 is found.</typeparam>
        /// <param name="sqlException">The original encompassing <see cref="SqlException"/> instance.</param>
        /// <returns>An exception of type <typeparamref name="TException"/> if a <see cref="SqlError"/> matching error number 5000 is found, otherwise null.</returns>
        public static TException ExtractUserDefinedSqlError<TException>(this SqlException sqlException) where TException : Exception, new()
        {
            return ExtractUserDefinedSqlError<TException>(sqlException, 50000);
        }

	    /// <summary>
	    /// Enumerates the <see cref="SqlErrorCollection"/> in the supplied <see cref="SqlException"/> and if user-defined error
        /// matching <paramref name="userDefinedErrorNumber"/> is encountered, then a new exception is created of the parameterised type <typeparamref name="TException"/>
	    /// and populated with the error message content of the user-defined SQL error. This can then be rethrown from the catch site to ensure the correct call stack.
	    /// </summary>
        /// <typeparam name="TException">The exception type to be created, populated and returned if a <see cref="SqlError"/> matching error number 5000 is found.</typeparam>
        /// <param name="sqlException">The original encompassing <see cref="SqlException"/> instance.</param>
        /// <param name="userDefinedErrorNumber">The user-defined error number to search for in the <see cref="SqlErrorCollection"/> errors collection.</param>
        /// <returns>An exception of type <typeparamref name="TException"/> if a <see cref="SqlError"/> matching error number 5000 is found, otherwise null.</returns>
	    public static TException ExtractUserDefinedSqlError<TException>(this SqlException sqlException, int userDefinedErrorNumber) where TException : Exception, new()
        {
            StringBuilder sb = new StringBuilder();

            foreach (SqlError sqlError in sqlException.Errors)
            {
                if (sqlError.Number == userDefinedErrorNumber)
                {
                    sb.AppendLine(sqlError.Message);
                }
            }

            return (sb.Length > 0)
                ? (TException)Activator.CreateInstance(typeof(TException), sb.ToString().Trim(), sqlException)
                : null;
        }

        /// <summary>
        /// Used to convert the supplied <see cref="NameValueCollection"/> into a delimited string suitable for use in diagnostics etc.
        /// </summary>
        /// <param name="nameValueCollection">The collection of items</param>
        /// <param name="delimiter">The delimiter to use between items</param>
        /// <returns>A single rendered string representing all keys and items in the collection.</returns>
        public static string ToString(this NameValueCollection nameValueCollection, string delimiter)
        {
            var renderedItemsList = new List<string>();

            for (int i = 0; i < nameValueCollection.Count; i++)
            {
                renderedItemsList.Add(String.Format("{0}: {1}", nameValueCollection.GetKey(i), nameValueCollection.Get(i)));
            }

            return String.Join(delimiter, renderedItemsList.ToArray());
        }

        /// <summary>
        /// Convert string to TitleCase. The first letter of every word always capitalised, and rest lowercase.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToTitleCase(this string value)
        {
            if(string.IsNullOrEmpty(value)) return value;

            var result = new StringBuilder();
            bool newWord = true;
            foreach(char c in value)
            {
                if(newWord) { result.Append(Char.ToUpper(c)); newWord = false; }
                else result.Append(Char.ToLower(c));
                if(c==' ') newWord = true;
            }
            return result.ToString();
        }

        /// <summary>
        /// Gets the value of src.propName
        /// </summary>
        /// <param name="src"></param>
        /// <param name="propName"></param>
        /// <returns></returns>
        public static object GetPropertyValue(this object src, string propName)
        {
            PropertyInfo property = src.GetType().GetProperty(propName);
            return property == null ? null : property.GetValue(src, null);
        }

        /// <summary>
        /// Sets the value for src.propName
        /// </summary>
        /// <param name="src"></param>
        /// <param name="propName"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static void SetPropertyValue(this object src, string propName, object val)
        {
            PropertyInfo property = src.GetType().GetProperty(propName);
            if (property != null)
            {
                property.SetValue(src, val);
            }
        }

	    /// <summary>
	    /// Calls the method of src object
	    /// </summary>
	    /// <param name="src"></param>
	    /// <param name="methodName"></param>
	    /// <param name="parameters"></param>
	    /// <returns></returns>
	    public static object InvokeMethod(this object src, string methodName, params object[] parameters)
	    {
	        MethodInfo method = src.GetType().GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
	        if (method == null)
	        {
	            throw new ArgumentException("Cannot find method " + methodName + " for the object " + src.ToString());
	        }

	        return method.Invoke(src, parameters);
        }

	    /// <summary>
        /// Rounds TimeSpan to nearest X minutes
        /// </summary>
        /// <param name="input"></param>
        /// <param name="minutes"></param>
        /// <returns></returns>
        public static TimeSpan RoundToNearestMinutes(this TimeSpan input, int minutes)
        {
            var totalMinutes = (int)(input + new TimeSpan(0, minutes / 2, 0)).TotalMinutes;

            return new TimeSpan(0, totalMinutes - totalMinutes % minutes, 0);
        }
    }
}
