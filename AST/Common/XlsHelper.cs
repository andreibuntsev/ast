﻿using System;
using System.IO;
using System.Windows;
using AST.DataModel;
using AST.Enums;
using NLog;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using HorizontalAlignment = NPOI.SS.UserModel.HorizontalAlignment;
using VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment;


namespace AST.Common
{
    public static class XlsHelper
    {
        //public static void dd()
        //{
        //    // Open Template
        //    FileStream fs = new FileStream(@".\XlsTemplates\TestOutputTemplate.xlsx", FileMode.Open, FileAccess.Read);

        //    // Load the template into a NPOI workbook
        //    XSSFWorkbook templateWorkbook = new XSSFWorkbook(fs);

        //    var ss = templateWorkbook.GetSheet("Output");

        //    // Load the sheet you are going to use as a template into NPOI
        //    var sheet = templateWorkbook.GetSheet("Output") as XSSFSheet;

            
        //    // Insert data into template
        //    //sheet.GetRow(0).GetCell(1).SetCellValue("olololo");  // Inserting a string value into Excel
        //    //sheet.GetRow(0).GetCell(2).SetCellValue("olololo");  // Inserting a string value into Excel
        //    //sheet.GetRow(1).GetCell(2).SetCellValue("olololo");  // Inserting a string value into Excel
        //    //sheet.GetRow(1).GetCell(1).SetCellValue("olololo");  // Inserting a string value into Excel



        //    //Set color
        //    //Row row = sheet.CreateRow(0);

        //    ////styling
        //    //Font boldFont = workbook.CreateFont();
        //    //boldFont.Boldweight = (short)FontBoldWeight.BOLD;
        //    //ICellStyle boldStyle = workbook.CreateCellStyle();
        //    //boldStyle.SetFont(boldFont);

        //    //boldStyle.BorderBottom = CellBorderType.MEDIUM;
        //    //boldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
        //    //boldStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;


        //    //for (int i = 0; i < columns.Length; i++)
        //    //{
        //    //    Cell cell = row.CreateCell(i);
        //    //    cell.SetCellValue(columns[i]);
        //    //    cell.CellStyle = boldStyle;
        //    //}



        //    //sheet.GetRow(3).GetCell(19).SetCellValue("olololo");


        //    // Save the NPOI workbook into a memory stream
        //    MemoryStream ms = new MemoryStream();
        //    templateWorkbook.Write(ms);

        //    FileStream file = new FileStream(@".\Output\test.xlsx", FileMode.Create, FileAccess.Write);
        //    ms.WriteTo(file);
        //    file.Close();
        //    ms.Close();
        //}


        public static void SaveAnalyzingReport(string refTrace, string testTrace, bool isSuccessful, string message)
        {
            LogHelper.LogInfo($"Trying to save xls output file for the test trace '{testTrace}'");
            string templatePath = $"{GlobalData.AssemblyDirectory}\\XlsTemplates\\TestOutputTemplate.xlsx";

            if (!File.Exists(templatePath))
            {
                throw new FileNotFoundException($"Cannot find the template file '{templatePath}'");
            }

            using (FileStream templateFileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read))
            {
                // Load the template into a NPOI workbook
                XSSFWorkbook templateWorkbook = new XSSFWorkbook(templateFileStream);

                // Load the sheet you are going to use as a template into NPOI
                var sheet = templateWorkbook.GetSheet("Output") as XSSFSheet;

                if (sheet == null)
                {
                    throw new InvalidOperationException($"Cannot find the 'Output' sheet in the template file '{templatePath}'");
                }

                // Insert data into the template
                sheet.GetRow(1).GetCell(3).SetCellValue(refTrace);
                sheet.GetRow(1).GetCell(0).SetCellValue(testTrace);

                ICellStyle resultStyle = templateWorkbook.CreateCellStyle();
                IFont boldFont = templateWorkbook.CreateFont();
                boldFont.Boldweight = (short)FontBoldWeight.Bold;
                resultStyle.VerticalAlignment = VerticalAlignment.Center;
                resultStyle.Alignment = HorizontalAlignment.Center;
                resultStyle.BorderBottom = BorderStyle.Thin;
                boldFont.Color = isSuccessful ? IndexedColors.Green.Index : IndexedColors.Red.Index;
                resultStyle.SetFont(boldFont);
                sheet.GetRow(1).GetCell(1).CellStyle = resultStyle;
                sheet.GetRow(1).GetCell(2).CellStyle = resultStyle;
                string successString = isSuccessful ? "SUCCESS" : "FAILED";
                sheet.GetRow(1).GetCell(1).SetCellValue(successString);
                sheet.GetRow(1).GetCell(2).SetCellValue(successString);

                if (!isSuccessful)
                {
                    sheet.GetRow(1).GetCell(4).SetCellValue($"Status: Failed; ERROR 1.\n\rComments: {message}");
                }
                    

                // Save the NPOI workbook into a memory stream
                using (MemoryStream outputMemoryStream = new MemoryStream())
                {
                    templateWorkbook.Write(outputMemoryStream);
                    string outputFileName = $".\\Output\\{testTrace}_{successString}_{DateTime.Now:yyyy-MM-dd-hhmmss}.xlsx";
                    LogHelper.LogInfo($"Trying to save xls output file '{outputFileName}'");
                    try
                    {
                        using (FileStream outputFile = new FileStream(outputFileName, FileMode.Create, FileAccess.Write))
                        {
                            outputMemoryStream.WriteTo(outputFile);
                            outputFile.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        LogHelper.LogError($"Failed to save xls output file '{outputFileName}', error: '{e.Message}', stack trace: '{e.StackTrace}'");
                        MessageBox.Show("Failed to save xls output file. Refer error log for details.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    

                    outputMemoryStream.Close();
                }
            }

                

            


            // Insert data into template
            //sheet.GetRow(0).GetCell(1).SetCellValue("olololo");  // Inserting a string value into Excel
            //sheet.GetRow(0).GetCell(2).SetCellValue("olololo");  // Inserting a string value into Excel
            //sheet.GetRow(1).GetCell(2).SetCellValue("olololo");  // Inserting a string value into Excel
            //sheet.GetRow(1).GetCell(1).SetCellValue("olololo");  // Inserting a string value into Excel



            //Set color
            //Row row = sheet.CreateRow(0);

            ////styling
            //Font boldFont = workbook.CreateFont();
            //boldFont.Boldweight = (short)FontBoldWeight.BOLD;
            //ICellStyle boldStyle = workbook.CreateCellStyle();
            //boldStyle.SetFont(boldFont);

            //boldStyle.BorderBottom = CellBorderType.MEDIUM;
            //boldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
            //boldStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;


            //for (int i = 0; i < columns.Length; i++)
            //{
            //    Cell cell = row.CreateCell(i);
            //    cell.SetCellValue(columns[i]);
            //    cell.CellStyle = boldStyle;
            //}



            //sheet.GetRow(3).GetCell(19).SetCellValue("olololo");


            
        }

    }
}
