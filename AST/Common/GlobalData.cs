﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AST.DataAccess;
using AST.DataModel;


namespace AST.Common
{
    public static class GlobalData
    {
        private static List<NetworkElement> _NetworkElements = SqliteDataAccess.LoadNetworkElements();

        
        public static List<NetworkElement> NetworkElements
        {
            get { return _NetworkElements; }
            set
            {
                _NetworkElements = value;
                SqliteDataAccess.UpdateNetworkElements(NetworkElements);
            }
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}