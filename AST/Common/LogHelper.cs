﻿using System;
using AST.DataModel;
using AST.Enums;
using NLog;


namespace AST.Common
{
    public static class LogHelper
    {
        private static Logger _Logger = LogManager.GetLogger(String.Empty);

        public static EventHandler<LogEvent> LogEventAdded;

        public static void LogInfo(string message)
        {
            _Logger.Log(LogLevel.Info, message);
            LogEventAdded?.Invoke(null, new LogEvent(message, LogEventLevel.Info, DateTime.Now));
        }

        public static void LogWarning(string message)
        {
            _Logger.Log(LogLevel.Warn, message);
            LogEventAdded?.Invoke(null, new LogEvent(message, LogEventLevel.Warning, DateTime.Now));
        }

        public static void LogError(string message)
        {
            _Logger.Log(LogLevel.Error, message);
            LogEventAdded?.Invoke(null, new LogEvent(message, LogEventLevel.Error, DateTime.Now));
        }
    }
}
