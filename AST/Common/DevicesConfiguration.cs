﻿using System;
using System.Configuration;


namespace AST.Common
{
    /// <summary>
    /// Main configuration section
    /// </summary>
    public class DevicesConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("devices")]
        [ConfigurationCollection(typeof(DeviceConfigElement), AddItemName = "device", CollectionType = ConfigurationElementCollectionType.BasicMap)]
        public ConfigurationElementCollection<DeviceConfigElement> Devices => base["devices"] as ConfigurationElementCollection<DeviceConfigElement>;
    }
    

    public class DeviceConfigElement : IdentifiableConfigurationElement
    {
        public override string Id => Type;

        [ConfigurationProperty("type")]
        public string Type => base["type"] as string;

        [ConfigurationProperty("touchActivities")]
        [ConfigurationCollection(typeof(TouchActivityConfigElement), AddItemName = "touchActivity", CollectionType = ConfigurationElementCollectionType.BasicMap)]
        public ConfigurationElementCollection<TouchActivityConfigElement> TouchActivities => base["touchActivities"] as ConfigurationElementCollection<TouchActivityConfigElement>;
    }


    public class TouchActivityConfigElement : IdentifiableConfigurationElement
    {
        public override string Id => Type.ToString();

        [ConfigurationProperty("type")]
        public ADBHelper.TouchActivityType Type => EnumHelper.GetAsEnum<ADBHelper.TouchActivityType>(base["type"]);

        [ConfigurationProperty("points")]
        [ConfigurationCollection(typeof(PointConfigElement), AddItemName = "point", CollectionType = ConfigurationElementCollectionType.BasicMap)]
        public ConfigurationElementCollection<PointConfigElement> Points => base["points"] as ConfigurationElementCollection<PointConfigElement>;
    }
    

    public class PointConfigElement : IdentifiableConfigurationElement
    {
        [ConfigurationProperty("x")]
        public int X => (int)base["x"];

        [ConfigurationProperty("y")]
        public int Y => (int)base["y"];

        [ConfigurationProperty("id")]
        public override string Id => base["id"].ToString();
    }


    #region Generic Implementation

    /// <summary>
    /// ConfigurationElement with additional Id property
    /// </summary>
    public abstract class IdentifiableConfigurationElement : ConfigurationElement
    {
        public virtual string Id { get; }
    }

    /// <summary>
    /// Generic ConfigurationElementCollection
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ConfigurationElementCollection<T> : ConfigurationElementCollection where T : IdentifiableConfigurationElement, new()
    {
        public T this[int index]
        {
            get { return (T)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(T serviceConfig)
        {
            BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new T();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((T)element).Id;
        }

        public void Remove(T serviceConfig)
        {
            BaseRemove(serviceConfig.Id);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(String name)
        {
            BaseRemove(name);
        }
    }

    #endregion //Generic Implementation

}
