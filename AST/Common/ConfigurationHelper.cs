﻿using System.Configuration;


namespace AST.Common
{
    public static class ConfigurationHelper
    {
        private static Configuration _Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);


        //ConnectionStrings
        public static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }


        //AppSettings
        public static string GetAppSetting(string name)
        {
            return _Configuration.AppSettings.Settings[name].Value;
        }

        public static void SetAppSetting(string name, string val)
        {
            _Configuration.AppSettings.Settings[name].Value = val;
            _Configuration.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
