﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using AST.DataModel;
using AST.Enums;


namespace AST.Common
{
    public static class TSharkHelper
    {
        #region Public Methods

        /// <summary>
        /// Fills packet list from a pcap file
        /// </summary>
        /// <param name="pcapFilePath"></param>
        /// <param name="filterCriteria"></param>
        public static List<Packet> LoadPackets(string pcapFilePath, FilterPcapFileCriteria filterCriteria)
        {
            //string xmlFile = "C:\\PCAPS\\cur_file";
            //TSharkHelper.ConvertPCAPFileToXml(pcapFilePath, xmlFile, filterCriteria);

            //var packets = ExtractPacketsFromXml(xmlFile);
            //return packets;


            string textFile = "C:\\PCAPS\\cur_file";
            TSharkHelper.ConvertPCAPFileToText(pcapFilePath, textFile, filterCriteria);

            var packets = ExtractPacketsFromText(textFile);
            return packets;
        }

        #endregion //Public Methods


        #region Private Methods

        /// <summary>
        /// Fills packet list from an xml file
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        private static List<Packet> ExtractPacketsFromXml(string xmlFile)
        {
            var packets = new List<Packet>();
            try
            {
                LogHelper.LogInfo($"FillPacketList: Started Parsing '{xmlFile}'");

                using (XmlReader reader = XmlReader.Create(xmlFile))
                {
                    reader.MoveToContent();

                    int n = 1;

                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name == "packet")
                            {
                                XElement packetElement = XNode.ReadFrom(reader) as XElement;
                                if (packetElement != null)
                                {
                                    List<XElement> sections = packetElement.Elements("section").ToList();
                                    if (sections[4].Value.Contains("SIP"))
                                    {
                                        //Check if (source == destination)
                                        if (sections[2].Value == sections[3].Value)
                                        {
                                            throw new InvalidOperationException(
                                                "Source cannot be equal to destination, PacketNo: " +
                                                sections[0].Value);
                                        }

                                        var newPacket = new Packet()
                                        {
                                            Sequence = n++,
                                            PacketNo = Int32.Parse(sections[0].Value),
                                            Time = sections[1].Value,
                                            Source = sections[2].Value,
                                            Destination = sections[3].Value,
                                            Protocol = sections[4].Value,
                                            Length = Int32.Parse(sections[5].Value),
                                        };
                                        packets.Add(newPacket);
                                    }
                                }
                            }
                        }
                    }

                    LogHelper.LogInfo($"FillPacketList: Finished Parsing '{xmlFile}'");
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"FillPacketList: Failed Parsing '{xmlFile}'. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Unhandled Error");
            }

            return packets;
        }

        /// <summary>
        /// Fills packet list from an xml file
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        private static List<Packet> ExtractPacketsFromText(string xmlFile)
        {
            var packets = new List<Packet>();
            StreamReader fileReader = null;
            string line = null;
            try
            {
                LogHelper.LogInfo($"FillPacketList: Started Parsing '{xmlFile}'");

                fileReader = new StreamReader(xmlFile);
                int n = 1;
                while ((line = fileReader.ReadLine()) != null)
                {
                    string[] fields = line.Split('\t');
                    
                    var newPacket = new Packet()
                    {
                        Sequence = n++,
                        PacketNo = Int32.Parse(fields[0]),
                        Time = fields[1].Replace(" AUS Eastern Daylight Time", ""),
                        Source = fields[2],
                        Destination = fields[3],
                        Protocol = fields[4],
                        //Length = Int32.Parse(sections[5].Value),
                        RequestType = fields[5],
                        ResponseType = fields[6],
                        CallID = fields[7],
                        CSeq = fields[8]
                    };
                    if (newPacket.PacketTypeEnum != PacketType.UNKNOWN)
                        packets.Add(newPacket);
                                    

                    LogHelper.LogInfo($"FillPacketList: Finished Parsing '{xmlFile}'");
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"FillPacketList: Failed Parsing '{xmlFile}'. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Unhandled Error");
            }
            finally
            {
                if (fileReader != null) fileReader.Close();
            }

            return packets;
        }

        private static void ConvertPCAPFileToXml(string pcapFilePath, string outputXmlFile, FilterPcapFileCriteria criteria)
        {
            if (criteria == null)
            {
                ConvertPCAPFileToXml(pcapFilePath, outputXmlFile);
                return;
            }

            try
            {
                if (String.IsNullOrEmpty(criteria.SipFrom))
                {
                    MessageBox.Show("sipFrom cannot be NULL", "Error");
                    LogHelper.LogError("sipFrom cannot be NULL");
                }

                if (String.IsNullOrEmpty(criteria.SipTo))
                {
                    MessageBox.Show("sipTo cannot be NULL", "Error");
                    LogHelper.LogError("sipTo cannot be NULL");
                }

                LogHelper.LogInfo(String.Format("TSharkHelper: Started Convertion '{0}' to '{1}' with arguments", pcapFilePath, outputXmlFile));

                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                string filterExpression = $" -Y \"sip.From contains {criteria.SipFrom.Substring(criteria.SipFrom.Length - 9, 9)} or sip.To contains {criteria.SipTo.Substring(criteria.SipTo.Length - 9, 9)}\"";

                process.StartInfo.Arguments = "/c " + ConfigurationHelper.GetAppSetting("TsharkFilePath") + " -r " + pcapFilePath + filterExpression + " -t ad -T psml > " + outputXmlFile;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();

                //C:\"Program Files"\Wireshark\tshark.exe -r C:\PCAPS\Call_A_to_B_good.pcap -Y "sip.From contains 426300897 or sip.To contains 426300342" -t ad -T fields -e frame.time -e ip.src -e ip.dst -e sip.Status-Code -e sip.Call-ID -e sip.CSeq.method > C:\PCAPS\psml_file
                LogHelper.LogInfo($"TSharkHelper: Completed Convertion '{pcapFilePath}' to '{outputXmlFile}'");
            }
            catch (Exception e)
            {
                LogHelper.LogInfo($"TSharkHelper: Failed conversion '{pcapFilePath}' to '{outputXmlFile}'. Error: {e.Message}");
                throw e;
            }
        }

        private static void ConvertPCAPFileToText(string pcapFilePath, string outputTextFile, FilterPcapFileCriteria criteria)
        {
            if (criteria == null)
            {
                ConvertPCAPFileToXml(pcapFilePath, outputTextFile);
                return;
            }

            try
            {
                if (String.IsNullOrEmpty(criteria.SipFrom))
                {
                    MessageBox.Show("sipFrom cannot be NULL", "Error");
                    LogHelper.LogError("sipFrom cannot be NULL");
                }

                if (String.IsNullOrEmpty(criteria.SipTo))
                {
                    MessageBox.Show("sipTo cannot be NULL", "Error");
                    LogHelper.LogError("sipTo cannot be NULL");
                }

                LogHelper.LogInfo($"TSharkHelper: Started Convertion '{pcapFilePath}' to '{outputTextFile}' with arguments");

                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                string from = criteria.SipFrom.Substring(criteria.SipFrom.Length - 9, 9);
                string to = criteria.SipTo.Substring(criteria.SipTo.Length - 9, 9);
                string filterExpression = $" -Y \"((sip.From contains {from} and sip.To contains {to}) or (sip.To contains {from} and sip.From contains {to})) and !(sip.Method==\"OPTIONS\")\"";

                process.StartInfo.Arguments = "/c " + ConfigurationHelper.GetAppSetting("TsharkFilePath") + " -r " + "\"" +  pcapFilePath + "\"" + filterExpression + " -t ad -T fields -e frame.number -e frame.time -e ip.src -e ip.dst -e frame.protocols -e sip.Method -e sip.Status-Code -e sip.Call-ID -e sip.CSeq.method > " + outputTextFile;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();

                //C:\"Program Files"\Wireshark\tshark.exe -r C:\PCAPS\Call_A_to_B_good.pcap -Y "sip.From contains 426300897 or sip.To contains 426300342" -t ad -T fields -e frame.time -e ip.src -e ip.dst -e sip.Status-Code -e sip.Call-ID -e sip.CSeq.method > C:\PCAPS\psml_file
                LogHelper.LogInfo($"TSharkHelper: Completed Convertion '{pcapFilePath}' to '{outputTextFile}'");
            }
            catch (Exception e)
            {
                LogHelper.LogInfo($"TSharkHelper: Failed conversion '{pcapFilePath}' to '{outputTextFile}'. Error: {e.Message}");
                throw e;
            }
        }

        private static void ConvertPCAPFileToXml(string pcapFilePath, string outputXmlFile)
        {
            try
            {
                LogHelper.LogInfo(String.Format("TSharkHelper: Started Convertion '{0}' to '{1}'", pcapFilePath, outputXmlFile));

                Process process = new Process();
                //string str = "";
                //string errstr = "";
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/c " + ConfigurationHelper.GetAppSetting("TsharkFilePath") + " -r " + pcapFilePath + " -t ad -T psml> " + outputXmlFile;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                //* Set your output and error (asynchronous) handlers
                //process.OutputDataReceived += (sender, args) => str += args.Data;
                //process.ErrorDataReceived += (sender, args) => errstr += args.Data;
                //* Start process and handlers
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();

                LogHelper.LogInfo($"TSharkHelper: Completed Convertion '{pcapFilePath}' to '{outputXmlFile}'");
            }
            catch (Exception e)
            {
                LogHelper.LogInfo($"TSharkHelper: Failed conversion '{pcapFilePath}' to '{outputXmlFile}'. Error: {e.Message}");
                throw e;
            }
        }

        private static void ConvertPCAPFileToText(string pcapFilePath, string outputXmlFile)
        {
            try
            {
                LogHelper.LogInfo($"TSharkHelper: Started Convertion '{pcapFilePath}' to '{outputXmlFile}'");

                //TODO: retorn error/info logging here
                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/c " + ConfigurationHelper.GetAppSetting("TsharkFilePath") + " -r " + pcapFilePath + " -t ad -T fields -e frame.number -e frame.time -e ip.src -e ip.dst -e frame.protocols -e sip.Request-Line -e sip.Status-Line -e sip.Call-ID > " + outputXmlFile;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();

                LogHelper.LogInfo($"TSharkHelper: Completed Convertion '{pcapFilePath}' to '{outputXmlFile}'");
            }
            catch (Exception e)
            {
                LogHelper.LogInfo($"TSharkHelper: Failed conversion '{pcapFilePath}' to '{outputXmlFile}'. Error: {e.Message}");
                throw e;
            }
        }

        #endregion //Private Methods
    }
}
