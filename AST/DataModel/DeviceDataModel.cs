﻿using System;
using System.Collections.Generic;
using AST.Common;
using SharpAdbClient;
using SharpAdbClient.DeviceCommands;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using AST.Base;


namespace AST.DataModel
{
    public class DeviceDataModel : Notifiable, IDisposable
    {
        #region Fields

        private DeviceData _DeviceData;
        
        private bool _Is4GEnabled;
        private bool _IsVoLTEEnabled;
        private bool _IsFlightModeEnabled;
        private string _PreviousCallStatus;
        private string _CallStatus;
        private bool _IsOutgoingCallAnswered;
        private string _IncomingCallNumber;

        private bool _IsRefreshEnabled = true;

        private string _DialNumberString;

        private Timer _DeviceStatusTimer;

        #endregion //Fields


        #region Constructors

        public DeviceDataModel(DeviceData deviceData)
        {
            _DeviceData = deviceData;
            
            try
            {
                DeviceProperties = deviceData.GetProperties();
                EnvironmentVariables = deviceData.GetEnvironmentVariables();
                DeviceKey = _DeviceData.Model + "###" + OSVersion;
                if (!this.IsConfigurationAvailable())
                {
                    //TODO: Nice Message box!  To investigate and update all others message boxes. What icon is the better? etc
                    MessageBoxResult result = MessageBox.Show(
                        $"Cannot find an appropriate configuration for the model '{_DeviceData.Model}' and OS Version '{OSVersion}'. Please contact vendor.\nYou can try running the device under the default configuration [Model: SM_G920I, OS Version: 6.0.1].",
                        "Configuration Error", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No, MessageBoxOptions.None);

                    IsEnabled = result == MessageBoxResult.Yes;
                    DeviceKey = "SM_G920I###6.0.1";
                }
                else
                {
                    _DeviceStatusTimer = new Timer(CheckStatusCallback, null, 0, 3000);
                    IsEnabled = true;
                }

                InitializeReceivers();
            }
            catch (Exception e)
            {
                LogHelper.LogError("DeviceDataModel Error: " + e.Message);
                IsEnabled = false;
            }
        }

        #endregion //Constructors


        #region Properties

        public DeviceData DeviceData
        {
            get { return _DeviceData; }
        }

        public string DeviceKey { get; private set; }
    

        public string OSVersion
        {
            get { return DeviceProperties["ro.build.version.release"]; }
        }

        public Dictionary<string, string> DeviceProperties { get; private set; }

        public Dictionary<string, string> EnvironmentVariables { get; private set; }

        public bool IsRefreshEnabled
        {
            get { return _IsRefreshEnabled; }
            set
            {
                if (_IsRefreshEnabled == value) return;
                _IsRefreshEnabled = value;
                OnPropertyChanged("IsRefreshEnabled");
            }
        }

        public bool Is4GEnabled
        {
            get { return _Is4GEnabled; }
            set
            {
                if (_Is4GEnabled == value) return;
                _Is4GEnabled = value;
                OnPropertyChanged("Is4GEnabled");
            }
        }

        public bool IsVoLTEEnabled
        {
            get { return _IsVoLTEEnabled; }
            set
            {
                if (_IsVoLTEEnabled == value) return;
                _IsVoLTEEnabled = value;
                OnPropertyChanged("IsVoLTEEnabled");
            }
        }

        public bool IsFlightModeEnabled
        {
            get { return _IsFlightModeEnabled; }
            set
            {
                if (_IsFlightModeEnabled == value) return;
                _IsFlightModeEnabled = value;
                OnPropertyChanged("IsFlightModeEnabled");
            }
        }

        public string CallStatus
        {
            get { return _CallStatus; }
            set
            {
                if (_CallStatus == value) return;
                _CallStatus = value;
                OnPropertyChanged("CallStatus");
            }
        }

        public bool IsOutgoingCallAnswered
        {
            get { return _IsOutgoingCallAnswered; }
            set
            {
                if (_IsOutgoingCallAnswered == value) return;
                _IsOutgoingCallAnswered = value;
                OnPropertyChanged("IsOutgoingCallAnswered");
            }
        }

        public string IncomingCallNumber
        {
            get { return _IncomingCallNumber; }
            set
            {
                if (_IncomingCallNumber == value) return;
                _IncomingCallNumber = value;
                OnPropertyChanged("IncomingCallNumber");
            }
        }

        //TODO: Remove. Used in device manager
        public string DialNumberString
        {
            get { return _DialNumberString; }
            set
            {
                if (_DialNumberString == value) return;
                _DialNumberString = value;
                OnPropertyChanged("DialNumber");
            }
        }

        //TODO: Do something with this...
        public Device ASTDevice { get; set; }

        public bool IsEnabled { get; set; }

        #endregion //Properties


        #region Methods

        public async Task ToggleFlightMode(bool notifyAboutWrongConfiguration)
        {
            await Task.Factory.StartNew(() =>
            {
                this.OpenSettings();
                this.ScrollUp();
                this.TouchAcivity(ADBHelper.TouchActivityType.ToggleFlightMode, notifyAboutWrongConfiguration);
            });
        }

        public async Task ToggleVOLTE(bool notifyAboutWrongConfiguration)
        {
            if (IsFlightModeEnabled)
            {
                MessageBox.Show("Cannot toggle VOLTE in Flight Mode!");
                return;
            }
            if (!Is4GEnabled)
            {
                MessageBox.Show("Cannot toggle VOLTE in '3G only' Mode!");
                return;
            }

            await Task.Factory.StartNew(() =>
            {
                this.OpenSettings();
                this.ScrollUp();
                this.TouchAcivity(ADBHelper.TouchActivityType.ToggleVOLTE, notifyAboutWrongConfiguration);
            });
        }

        public async Task Enable3G(bool notifyAboutWrongConfiguration)
        {
            if (IsFlightModeEnabled)
            {
                MessageBox.Show("Cannot toggle VOLTE in Flight Mode!");
                return;
            }

            if (!Is4GEnabled)
            {
                MessageBox.Show("'3G only' mode is already enabled!");
                return;
            }

            await Task.Factory.StartNew(() =>
            {
                this.OpenSettings();
                this.ScrollUp();
                this.TouchAcivity(ADBHelper.TouchActivityType.Enable3G, notifyAboutWrongConfiguration);
            });
        }

        public async Task Enable4G(bool notifyAboutWrongConfiguration)
        {
            if (IsFlightModeEnabled)
            {
                MessageBox.Show("Cannot toggle VOLTE in Flight Mode!");
                return;
            }

            if (Is4GEnabled)
            {
                MessageBox.Show("4G mode is already enabled!");
                return;
            }

            await Task.Factory.StartNew(() =>
            {
                this.OpenSettings();
                this.ScrollUp();
                this.TouchAcivity(ADBHelper.TouchActivityType.Enable4G, notifyAboutWrongConfiguration);
            });
        }

        public async Task Reboot()
        {
            await Task.Factory.StartNew(() => AdbClient.Instance.Reboot(DeviceData));
        }
        
        public async Task DialNumber(string numberString)
        {
            if (String.IsNullOrEmpty(numberString))
            {
                MessageBox.Show("The number to dial is empty");
                LogHelper.LogError("The number to dial is empty");
                return;
            }


            await Task.Factory.StartNew(() =>
            {
                this.MinimizeAllApps();
                this.TouchAcivity(ADBHelper.TouchActivityType.OpenDialer);
                Thread.Sleep(1000);
                //_DeviceData.ExecuteShellCommand("input tap 1200 2350", null);//Open Keypad

                //Split the number into separate symbols
                foreach (char c in numberString)
                {
                    switch (c)
                    {
                        case '1':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial1);
                            break;
                        case '2':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial2);
                            break;
                        case '3':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial3);
                            break;
                        case '4':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial4);
                            break;
                        case '5':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial5);
                            break;
                        case '6':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial6);
                            break;
                        case '7':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial7);
                            break;
                        case '8':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial8);
                            break;
                        case '9':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial9);
                            break;
                        case '*':
                            this.TouchAcivity(ADBHelper.TouchActivityType.DialAsterisk);
                            break;
                        case '0':
                            this.TouchAcivity(ADBHelper.TouchActivityType.Dial0);
                            break;
                        case '#':
                            this.TouchAcivity(ADBHelper.TouchActivityType.DialHash);
                            break;
                        case '+':
                            this.PutPlus();
                            break;
                    }
                }

                this.TouchAcivity(ADBHelper.TouchActivityType.StartCall);
            });
        }

        public async Task AbortCall()
        {
            await Task.Factory.StartNew(() =>
            {
                this.TouchAcivity(ADBHelper.TouchActivityType.AbortCall);
            });
        }

        public async Task AcceptCall()
        {
            await Task.Factory.StartNew(() => _DeviceData.ExecuteShellCommand("input keyevent 5", null));
        }


        #region CheckStatusesStuff

        private bool _IsWaitingForAnswer = false;
        //TODO: Use Cancellation Token
        private CancellationToken _CancellationToken = new CancellationToken();
        private ASTShellOutputReceiver _EmptyReceiver = new ASTShellOutputReceiver();
        private ASTShellOutputReceiver _Output4GReceiver = new ASTShellOutputReceiver();
        private ASTShellOutputReceiver _OutputVoLTEReceiver = new ASTShellOutputReceiver();
        private ASTShellOutputReceiver _OutputFlightModeReceiver = new ASTShellOutputReceiver();
        private ASTShellOutputReceiver _CallStatusReceiver = new ASTShellOutputReceiver();
        private ASTShellOutputReceiver _CallIncomingNumberReceiver = new ASTShellOutputReceiver();
        private ASTShellOutputReceiver _AwaitAnswerReceiver = new ASTShellOutputReceiver();

        private async void CheckStatusCallback(object obj)
        {
            if (!IsRefreshEnabled) return;

            //Get 4G status
            try
            {
                await ExecuteAdbCommand("settings get global preferred_network_mode", _Output4GReceiver);
            }
            catch (Exception e)
            {
                LogHelper.LogError("DeviceDataModel.CheckStatusCallback() Error: Get 4G status: " + e.Message);
                //_DeviceStatusTimer.Dispose();
            }


            //Get VoLTE status
            try
            {
                await ExecuteAdbCommand("settings get system voicecall_type", _OutputVoLTEReceiver);
            }
            catch (Exception e)
            {
                LogHelper.LogError("DeviceDataModel.CheckStatusCallback() Error: Get VoLTE status: " + e.Message);
                //_DeviceStatusTimer.Dispose();
            }


            //Get FlightMode status
            try
            {
                await ExecuteAdbCommand("settings get global airplane_mode_on", _OutputFlightModeReceiver);
            }
            catch (Exception e)
            {
                LogHelper.LogError("DeviceDataModel.CheckStatusCallback() Error: Get Flight Mode status: " + e.Message);
                //_DeviceStatusTimer.Dispose();
            }

            //Get call status
            try
            {
                await ExecuteAdbCommand("dumpsys telephony.registry | grep -m 1 mCallState", _CallStatusReceiver);
            }
            catch (Exception e)
            {
                LogHelper.LogError("DeviceDataModel.CheckStatusCallback() Error: Get Call status: " + e.Message);
                //_DeviceStatusTimer.Dispose();
            }
        }


        private void InitializeReceivers()
        {
            _Output4GReceiver.OutputAdded += Output4GReceiver_OutputAdded;
            _OutputVoLTEReceiver.OutputAdded += OutputVoLteReceiver_OutputAdded;
            _OutputFlightModeReceiver.OutputAdded += OutputFlightModeReceiver_OutputAdded;
            _CallStatusReceiver.OutputAdded += CallStatusReceiver_OutputAdded;
            _CallIncomingNumberReceiver.OutputAdded += CallIncomingNumberReceiver_OutputAdded;
            _AwaitAnswerReceiver.OutputAdded += AwaitAnswerReceiver_OutputAdded;
        }

        
        private void Output4GReceiver_OutputAdded(object sender, string output)
        {
            Is4GEnabled = !String.IsNullOrEmpty(output) && output.Substring(0, 1) == "9"; //2 is for 3G only
        }

        private void OutputVoLteReceiver_OutputAdded(object sender, string output)
        {
            IsVoLTEEnabled = output == "0";
        }

        private void OutputFlightModeReceiver_OutputAdded(object sender, string output)
        {
            IsFlightModeEnabled = output == "1";
        }

        private void CallStatusReceiver_OutputAdded(object sender, string output)
        {
            if (!String.IsNullOrEmpty(output))
            {
                string status = output.Replace(" mCallState=", "").Substring(0, 2).Trim();
                switch (status)
                {
                    case "0":
                        CallStatus = "Idle";
                        IncomingCallNumber = null;
                        IsOutgoingCallAnswered = false;
                        _IsWaitingForAnswer = false;
                        _PreviousCallStatus = CallStatus;
                        break;
                    case "1":
                        CallStatus = "Incoming";
                        _IsWaitingForAnswer = false;
                        IsOutgoingCallAnswered = false;
                        //Get Incoming Call Nnumber
                        try
                        {
                            ExecuteAdbCommand("dumpsys telephony.registry | grep -m 1 mCallIncomingNumber", _CallIncomingNumberReceiver).GetAwaiter().GetResult();
                        }
                        catch (Exception e)
                        {
                            LogHelper.LogError("DeviceDataModel.CheckStatusCallback() Error: Get Incoming Call Nnumber: " + e.Message);
                            //_DeviceStatusTimer.Dispose();
                        }
                        _PreviousCallStatus = CallStatus;
                        break;
                    case "2":
                        if (_PreviousCallStatus == "Incoming") break;
                        CallStatus = "Outgoing";
                        //TODO: Set a timeout or cancellation token for the awaiter
                        Task.Factory.StartNew(SetAnswerAwaiter);
                        _PreviousCallStatus = CallStatus;
                        break;
                    default:
                        LogHelper.LogInfo($"Call status for the device '{_DeviceData.Model}' received: {output}");
                        return;
                }

                LogHelper.LogInfo($"Call status for the device '{_DeviceData.Model}' received: {CallStatus}");
            }
        }

        private void CallIncomingNumberReceiver_OutputAdded(object sender, string output)
        {
            if (!String.IsNullOrEmpty(output) && !output.StartsWith("Failed"))
            {
                string number = output.Replace(" mCallIncomingNumber=", "").Substring(0).Trim();
                if (!String.IsNullOrEmpty(number))
                {
                    IncomingCallNumber = number;
                    LogHelper.LogInfo($"Incoming phone number to the device '{_DeviceData.Model}' received: {number}");
                }
                else
                {
                    LogHelper.LogWarning($"Incoming phone number to the device '{_DeviceData.Model}' is NULL");
                }
            }
        }

        private void AwaitAnswerReceiver_OutputAdded(object sender, string s)
        {
            IsOutgoingCallAnswered = true;
        }

        private async Task ExecuteAdbCommand(string command, ASTShellOutputReceiver receiver)
        {
            //Guid guid = Guid.NewGuid();
            DateTime startTime = DateTime.Now;
            receiver.RequestStarTime = startTime;
            //LogHelper.LogInfo($"Command '{command}' execution started against the device '{_DeviceData.Model}'. ID: '{guid}'");
            await AdbClient.Instance.ExecuteRemoteCommandAsync(command, _DeviceData, receiver, _CancellationToken, 100);
            //LogHelper.LogInfo($"Command '{command}' execution completed against the device '{_DeviceData.Model}'. ID: '{guid}'");
            double durationMilliseconds = (DateTime.Now - startTime).TotalMilliseconds;
            if (durationMilliseconds > 3000)
            {
                LogHelper.LogWarning($"Command '{command}' execution against the device '{_DeviceData.Model}' has taken '{durationMilliseconds}' milliseconds.");
            }
        }

        private async void SetAnswerAwaiter()
        {
            if (_IsWaitingForAnswer) return;
            _IsWaitingForAnswer = true;

            //Get call status
            try
            {
                await ExecuteAdbCommand("logcat -c", _EmptyReceiver);
                await ExecuteAdbCommand("logcat | grep \"Telephony: GsmConnection: onStateChanged, state: ACTIVE\"", _AwaitAnswerReceiver);
            }
            catch (Exception e)
            {
                LogHelper.LogError("DeviceDataModel.SetAnswerAwaiter() Error: " + e.Message);
                //_DeviceStatusTimer.Dispose();
            }
        }

        #endregion //CheckStatusesStuff


        public void Dispose()
        {
            _DeviceStatusTimer.Dispose();
            _Output4GReceiver.OutputAdded -= Output4GReceiver_OutputAdded;
            _OutputVoLTEReceiver.OutputAdded -= OutputVoLteReceiver_OutputAdded;
            _OutputFlightModeReceiver.OutputAdded -= OutputFlightModeReceiver_OutputAdded;
            _CallStatusReceiver.OutputAdded -= CallStatusReceiver_OutputAdded;
            _CallIncomingNumberReceiver.OutputAdded -= CallIncomingNumberReceiver_OutputAdded;
            _AwaitAnswerReceiver.OutputAdded -= AwaitAnswerReceiver_OutputAdded;
        }




        #endregion //Methods
    }
}
