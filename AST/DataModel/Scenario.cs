﻿using System;
using AST.Common;
using AST.Enums;

namespace AST.DataModel
{
    public class Scenario
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
        
        public string Description { get; set; }

        public string DeviceANetworkType { get; set; }

        public string DeviceBNetworkType { get; set; }

        public int DeviceARestartBeforeTest { get; set; }

        public int DeviceBRestartBeforeTest { get; set; }

        public int DeviceAFlightMode { get; set; }

        public int DeviceBFlightMode { get; set; }

        public int IsLocalCall { get; set; }

        public int CallDurationSeconds { get; set; }

        public string ReferenceCallFlowFileName { get; set; }


        public ScenarioType TypeEnum
        {
            get { return EnumHelper.GetAsEnum<ScenarioType>(Type); }
            set { Type = value.ToString(); }
        }

        public DeviceNetworkType? DeviceANetworkTypeEnum
        {
            get { return String.IsNullOrEmpty(DeviceANetworkType) ? (DeviceNetworkType?)null : EnumHelper.GetAsEnum<DeviceNetworkType>(DeviceANetworkType); }
            set { DeviceANetworkType = value.ToString(); }
        }

        public DeviceNetworkType? DeviceBNetworkTypeEnum
        {
            get { return String.IsNullOrEmpty(DeviceBNetworkType) ? (DeviceNetworkType?)null : EnumHelper.GetAsEnum<DeviceNetworkType>(DeviceBNetworkType); }
            set { DeviceBNetworkType = value.ToString(); }
        }
    }
}
