﻿using System;
using AST.Base;
using AST.ViewModel;


namespace AST.DataModel
{
    public class MessageFlowModel : Notifiable //TODO: Split DataContract and ViewModel
    {
        //public const int COLUMN_WIDTH = 150;

        private bool _IsSelected;
        private bool _IsCallIdTextBoxMode;

        public event EventHandler RefreshFlowEvent;

        public MessageFlowModel(Packet packet, MessagesViewModel messagesViewModel)
        {
            MessagesViewModel = messagesViewModel;

            Packet = packet;
            Sequence = packet.Sequence;
            PacketNo = packet.PacketNo;
            PacketId = packet.Id;
            Source = packet.Source;
            Destination = packet.Destination;
            Protocol = packet.Protocol;
            PacketType = packet.PacketType;
            Length = packet.Length;
            CallID = packet.CallID;

            FlowDirection = MessagesViewModel.NodeList.IndexOf(Source) < MessagesViewModel.NodeList.IndexOf(Destination) ? AST.Enums.FlowDirection.Right : AST.Enums.FlowDirection.Left;
        }

        public MessagesViewModel MessagesViewModel { get; set; }

        public AST.Enums.FlowDirection FlowDirection { get; set; }

        public int Sequence { get; set; }

        public Packet Packet { get; set; }

        public int PacketNo { get; set; }

        public int PacketId { get; set; }

        //public DateTime Time { get; set; }

        public string Source { get; set; }

        public string Destination { get; set; }

        public string Protocol { get; set; }

        public int Length { get; set; }

        public string CallID { get; set; }

        public string PacketType { get; set; }

        public bool IsRelated { get; set; }

        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (_IsSelected == value) return;
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public bool IsCallIdTextBoxMode
        {
            get { return _IsCallIdTextBoxMode; }
            set
            {
                if (_IsCallIdTextBoxMode == value) return;
                _IsCallIdTextBoxMode = value;
                OnPropertyChanged("IsCallIdTextBoxMode");
            }
        }

        public void RefreshFlow()
        {
            if (RefreshFlowEvent != null)
                RefreshFlowEvent(this, EventArgs.Empty);
        }
    }
}
