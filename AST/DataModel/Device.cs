﻿
namespace AST.DataModel
{
    public class Device
    {
        public int? Id { get; set; }

        public string Serial { get; set; }

        public string Model { get; set; }

        public string OSVersion { get; set; }

        public string Number { get; set; }
    }
}
