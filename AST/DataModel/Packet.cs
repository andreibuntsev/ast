﻿
using System;
using System.Collections.Generic;
using System.Linq;
using AST.Common;
using AST.Enums;

namespace AST.DataModel
{
    public class Packet : ICloneable
    {
        public int Sequence { get; set; }
        
        public int Id { get; set; }

        public int ScenarioId { get; set; }

        public int PacketNo { get; set; }

        public string Time { get; set; }

        public string Source { get; set; }

        public string Destination { get; set; }

        public string Protocol { get; set; }

        public int Length { get; set; }

        public string RequestType { get; set; }

        public string ResponseType { get; set; }

        public int _4XX_6XXNumber { get; set; }

        public string CallID { get; set; }

        public string CSeq { get; set; }

        public bool IsRedHighlighted { get; set; }


        public string PacketType
        {
            get { return !String.IsNullOrEmpty(RequestType) ? RequestType : ResponseType; }
        }

        public PacketType PacketTypeEnum
        {
            get
            {
                try
                {
                    if (Int32.TryParse(PacketType, out var parsedPacketType) && (parsedPacketType >= 400) && (parsedPacketType <= 699))
                    {
                        _4XX_6XXNumber = parsedPacketType;
                        return Enums.PacketType._4XX_6XX;
                    }
                    return (PacketType) (EnumHelper.GetEnum(typeof(PacketType), PacketType));
                }
                catch (Exception)
                {
                    return Enums.PacketType.UNKNOWN;
                }
            }
            //set { PacketType = value.ToString(); }
        }

        public NetworkElement SourceNetworkElement
        {
            get
            {
                NetworkElement sourceNetworkElement = GlobalData.NetworkElements.FirstOrDefault(ne => ne.IPAddress == Source);
                return sourceNetworkElement;
            }
        }

        public NetworkElement DestinatioNetworkElement
        {
            get
            {
                NetworkElement destinationNetworkElement = GlobalData.NetworkElements.FirstOrDefault(ne => ne.IPAddress == Destination);
                return destinationNetworkElement;
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
