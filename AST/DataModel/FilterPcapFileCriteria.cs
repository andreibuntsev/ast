﻿using System;


namespace AST.DataModel
{
    public class FilterPcapFileCriteria
    {
        public string SipFrom { get; set; }

        public string SipTo { get; set; }

        public DateTime? TimeFrom { get; set; }

        public DateTime? TimeTo { get; set; }
    }
}
