﻿using System;
using AST.Base;
using AST.Enums;


namespace AST.DataModel
{
    public class LogEvent : Notifiable
    {
        #region Fields

        private bool _IsVisible;

        #endregion //Fields


        #region Constructor

        public LogEvent(string message, LogEventLevel level, DateTime timeStamp)
        {
            Message = message;
            Level = level;
            TimeStamp = timeStamp;
        }

        #endregion //Constructor


        #region Properties

        public string Message { get; private set; }

        public LogEventLevel Level { get; private set; }

        public DateTime TimeStamp { get; private set; }

        public bool IsVisible
        {
            get { return _IsVisible; }
            set
            {
                if (_IsVisible == value) return;
                _IsVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        #endregion //Properties
    }
}
