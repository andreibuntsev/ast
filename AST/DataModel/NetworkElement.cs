﻿using AST.Common;
using AST.Enums;

namespace AST.DataModel
{
    public class NetworkElement
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public NetworkElementType TypeEnum
        {
            get { return EnumHelper.GetAsEnum<NetworkElementType>(Type); }
            set { Type = value.ToString(); }
        }

        public string DomainName { get; set; }

        public string IPAddress { get; set; }

        //TODO: Add type to enum
    }
}
