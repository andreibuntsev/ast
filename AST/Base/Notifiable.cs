﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;


namespace AST.Base
{
    public class Notifiable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Marshals a call onto the UI thread with the specified dispatcher priority.
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="dispatcherPriority"></param>
        public static void QueueForUIThreadStatic(ThreadStart callback, DispatcherPriority dispatcherPriority)
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(dispatcherPriority, callback);
        }

        /// <summary>
        /// Puts a call onto a background thread
        /// </summary>
        /// <param name="callback"></param>
        public static void QueueForBackgroundThreadStatic(ThreadStart callback)
        {
            ThreadPool.QueueUserWorkItem
            (
                (
                    delegate
                    {
                        try
                        {
                            callback();
                        }
                        catch (Exception e)
                        {
                            //if an exception occured, then marshal it onto the UI thread before re-throwing
                            QueueForUIThreadStatic
                            (
                                delegate { throw new Exception(e.Message, e); }, DispatcherPriority.Background
                            );
                        }
                    }
                )
            );
        }
    }
}
